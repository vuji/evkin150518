-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 30, 2018 at 02:04 PM
-- Server version: 10.3.7-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siekinbaru`
--

-- --------------------------------------------------------

--
-- Table structure for table `aspek`
--

CREATE TABLE `aspek` (
  `id_aspek` float NOT NULL,
  `nama_aspek` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aspek`
--

INSERT INTO `aspek` (`id_aspek`, `nama_aspek`) VALUES
(1, 'Aspek Keuangan'),
(2, 'Aspek Pelayanan');

-- --------------------------------------------------------

--
-- Table structure for table `datasurat`
--

CREATE TABLE `datasurat` (
  `id_surat` int(11) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `nama_unit` varchar(255) DEFAULT NULL,
  `no_surat_usul` varchar(255) DEFAULT NULL,
  `tgl_surat_usul` date DEFAULT NULL,
  `no_sk` varchar(255) DEFAULT NULL,
  `tgl_sk` date DEFAULT NULL,
  `tgl_tmt` date DEFAULT NULL,
  `catatan` text DEFAULT NULL,
  `file` text DEFAULT NULL,
  `id_jenissk` int(11) DEFAULT NULL,
  `pemroses` varchar(255) DEFAULT NULL,
  `ksb` varchar(255) DEFAULT NULL,
  `tgl_reviu_ksb` date DEFAULT NULL,
  `kabag` varchar(255) DEFAULT NULL,
  `tgl_reviu_kabag` date DEFAULT NULL,
  `karo` varchar(255) DEFAULT NULL,
  `tgl_reviu_karo` date DEFAULT NULL,
  `sesma` varchar(255) DEFAULT NULL,
  `tgl_reviu_sesma` date DEFAULT NULL,
  `kepala` varchar(255) DEFAULT NULL,
  `tgl_reviu_kepala` date DEFAULT NULL,
  `status` enum('upload','selesai','proses') CHARACTER SET latin2 DEFAULT 'proses',
  `is_active` enum('0','1') DEFAULT '1',
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datasurat`
--

INSERT INTO `datasurat` (`id_surat`, `nip`, `nama_unit`, `no_surat_usul`, `tgl_surat_usul`, `no_sk`, `tgl_sk`, `tgl_tmt`, `catatan`, `file`, `id_jenissk`, `pemroses`, `ksb`, `tgl_reviu_ksb`, `kabag`, `tgl_reviu_kabag`, `karo`, `tgl_reviu_karo`, `sesma`, `tgl_reviu_sesma`, `kepala`, `tgl_reviu_kepala`, `status`, `is_active`, `created_date`) VALUES
(7, '195401281975071001', 'Pusat Pendidikan dan Pelatihan Pengawasan', '111', '2018-01-01', '666', '2018-01-04', '2018-01-12', 'sudah oke', '2018-01-24-01.Template_Peraturan_Kepala_BPKP_Maret2017_.docx', 6, 'Acih', '1', '2018-01-01', '333', '2018-01-23', 'ttg', NULL, 'V', NULL, '0', NULL, 'selesai', '1', '2018-01-18'),
(8, '195801041979112001', 'Perwakilan BPKP Provinsi Jawa Barat', '11/23/12222', '2016-12-13', '990.213.123', '2018-01-06', '2018-01-11', '', '2018-01-26-Lamp._Perka_BPKP_16_2017_Tukin_._Berka_BPKP_16_2017__Tukin_.pdf', 53, 'Warsito', '2', '2018-01-03', '222', '2018-01-02', 'ttg', '2018-01-03', 'V', '2018-01-03', 'V', '2018-01-03', 'selesai', '1', '2018-01-20'),
(9, '196705071994032001', 'Biro Umum', '1111', '2016-11-30', NULL, NULL, NULL, NULL, NULL, 23, 'Kuwat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'proses', '1', '2018-01-24'),
(10, '195901031979111001', 'Perwakilan BPKP Provinsi Jawa Tengah', '991.231.222', '2016-12-13', NULL, NULL, NULL, NULL, NULL, 57, 'Kuwat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'proses', '1', '2018-01-24'),
(11, '196802061991031001', 'Deputi Bidang Pengawasan Penyelenggaraan Keuangan Daerah', 'S-469/D504/2016', '2016-12-09', NULL, NULL, NULL, NULL, NULL, 31, 'Warsito', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'proses', '1', '2018-01-26'),
(12, '196901101990031001', 'Biro Kepegawaian dan Organisasi', 'KEP-8/K.SU/02/2017', '2017-01-05', NULL, NULL, NULL, NULL, NULL, 31, 'Acih', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'proses', '1', '2018-01-26'),
(14, '195812051980121001', 'Perwakilan BPKP Provinsi Sumatera Barat', 'S-469/D504/2018', '2016-12-13', 'KEP-431/K./SU/2018', '2018-01-31', '2018-02-01', '', '', 65, 'Kuwat', '1', '2018-01-25', 'Angka S', '2018-01-29', 'ttg', '2018-01-30', '0', NULL, '0', NULL, 'upload', '1', '2018-01-26');

-- --------------------------------------------------------

--
-- Table structure for table `indikator`
--

CREATE TABLE `indikator` (
  `id_indikator` float NOT NULL,
  `nama_indikator` varchar(155) DEFAULT NULL,
  `bobot` double DEFAULT 0,
  `id_subaspek` varchar(11) DEFAULT NULL,
  `id_kelindikator` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `indikator`
--

INSERT INTO `indikator` (`id_indikator`, `nama_indikator`, `bobot`, `id_subaspek`, `id_kelindikator`) VALUES
(1, 'Raiso Kas (Cash Ratio)', 2, '1', ''),
(2, 'Rasio Lancar (Curent Ratio)', 2.5, '1', ''),
(3, 'Periode Penagihan Piutang ( Collection Period)', 2, '1', ''),
(4, 'Perputaran Aset Tetap (Fixed Asset Turnover)', 2, '1', ''),
(5, 'lmbalan atas Aset Tetap (Return on Fixed Asset)', 2, '1', ''),
(6, 'Imbalan Ekuitas (Return on Equity)', 2, '1', ''),
(7, 'Perputaran Persediaan (Inventory Turnover)', 2, '1', ''),
(8, 'Rasio Pendapatan PNBP terhadap Biaya Operasional', 2.5, '1', ''),
(9, 'Rasio Subsidi Biaya Pasien', 2, '1', ''),
(10, 'Rencana Bisnis dan Anggaran (RBA) Definitif', 2, '2', ''),
(11, 'Laporan Keuangan Berdasarkan Standar Akuritansi Keuangan', 2, '2', ''),
(12, 'Surat Perintah Pengesahan Peridapatan dan Belanja BLC', 2, '2', ''),
(13, 'Tarif Layanan', 1, '2', ''),
(14, 'Sistem Akuntansi', 1, '2', ''),
(15, 'Persetujuan Rekening', 0.5, '2', ''),
(16, 'SOP Pengelolaan Kas', 0.5, '2', ''),
(17, 'SOP Pengelolaan Piutang', 0.5, '2', ''),
(18, 'SOP Pengelolaan Utang', 0.5, '2', ''),
(19, 'SOP Pengadaan Barang dan Jasa', 0.5, '2', ''),
(20, 'SOP Pengelolaan Barang Inventaris', 0.5, '2', ''),
(21, 'Periode Penagihan Piutang ( Collection Period)', 2, '1', ''),
(22, 'Pertumbuhan Produktivitas', 0, '3', ''),
(23, 'Pertumbuhan Rata-rata Kunjungan Rawat Jalan', 3, '3', '22'),
(24, 'Pertumbuhan Rata-rata Kunjurigan Rawat Darurat', 2.5, '3', '22'),
(25, 'Pertumbuhan Hari Perawatan Rawat Inap', 2.5, '3', '22'),
(26, 'Pertumbuhan Pemeriksaan Radiologi', 2.5, '3', '22'),
(27, 'Pertumbuhan Pemeriksaan Radiologi', 2.5, '3', '22'),
(28, 'Pertumbuhan Pemeriksaan Laboratoriurn', 2.5, '3', '22'),
(29, 'Pertumbuhan Operasi', 2.5, '3', '22'),
(30, 'Pertumbuhan Rehab Medik', 2.5, '3', '22'),
(31, 'Efektivitas Pelayanan', 0, '3', ''),
(32, 'Kelengkapan Rekam Medik 24 Jam Selesai Pelayanan', 2, '3', '31'),
(33, 'Pengembalian Rekam Medik', 2, '3', '31'),
(34, 'Angka Pernbatalan Operasi', 2, '3', '31'),
(35, 'Angka Kegagalan Hasil Radiologi', 2, '3', '31'),
(36, 'Penulisan Resep Sesuai Fonnularium', 2, '3', '31'),
(37, 'Angka Pengulangan Pemeriksaan Laboratorium', 2, '3', '31'),
(38, 'Bed Occupancy Rate (BOR)', 2, '3', '31'),
(39, 'Pertumbuhan Pembelajaran', 0, '3', ''),
(40, 'Rata-rata Jam Pelatihan/Karyawan', 1.5, '3', '39'),
(41, 'Program Reward and Punishment', 1.5, '3', '39'),
(42, 'Mutu Pelayanan', 0, '4', ''),
(43, 'Emergency Response Time Rate', 2, '4', '42'),
(44, 'Waktu Tunggu Rawat Jalan', 2, '4', '42'),
(45, 'Length of Stay', 2, '4', '42'),
(46, 'Kecepatan Pelayanan Resep Obat Jadi', 2, '4', '42'),
(47, 'Waktu Tunggu Sebelum Operasi', 2, '4', '42'),
(48, 'Waktu Tunggu Hasil Laboratorium', 2, '4', '42'),
(49, 'Waktu Tunggu Hasil Radiologi', 2, '4', '42'),
(50, 'Mutu Klinik', 0, '4', ''),
(51, 'Angka Kematian di Gawat Darurat', 2, '4', '50'),
(52, 'Angka Kematian/Kebutaan ? 48 Jam', 2, '4', '50'),
(53, 'Post Operative Death Rate', 2, '4', '50'),
(54, 'Angka Infeksi Nosokomial', 4, '4', '50'),
(55, 'Angka Kematian Ibu di Rurnah Sakit', 2, '4', '50'),
(56, 'Kepedulian Kepada Masyarakat', 0, '4', ''),
(57, 'Pembinaan Kepada Pusat Kesehatan Masyarakat dan Sarana Kesehatan Lain', 1, '4', '56'),
(58, 'Penyuluhan Kesehatan', 1, '4', '56'),
(59, 'Rasio Tempat Tidur Kelas III', 2, '4', '56'),
(60, 'Kepuasan Pelanggan', 0, '4', ''),
(61, 'Penanganan Pengaduan/Komplain', 1, '4', '60'),
(62, 'Kepuasan Pelanggan', 1, '4', '60'),
(63, 'Kepedulian Terhadap Lingkungan', 0, '4', ''),
(64, 'Kebersihan Lingkungan (Program Rumah Sakit Berseri)', 2, '4', '63'),
(65, 'Proper Lingkungan', 1, '4', '63');

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id_penilaian` int(11) NOT NULL,
  `id_rsd` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `id_indikator` int(11) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `capaian` float DEFAULT NULL,
  `persentase` float NOT NULL,
  `penyebab` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id_penilaian`, `id_rsd`, `tahun`, `id_indikator`, `nilai`, `capaian`, `persentase`, `penyebab`) VALUES
(1, 1, 2016, 23, 3, 4, 0.75, 'nightcore'),
(2, 1, 2016, 30, 2.5, 4, 0.625, '4daycore'),
(3, 1, 2016, 29, 2.5, 4, 0.625, '42'),
(4, 1, 2016, 28, 2.5, 4, 0.625, '42'),
(5, 1, 2016, 27, 2.5, 4, 0.625, '42'),
(6, 1, 2016, 26, 2.5, 4, 0.625, '442'),
(7, 1, 2016, 25, 2.5, 4, 0.625, '442'),
(8, 1, 2016, 24, 2.5, 4, 0.625, '442'),
(9, 1, 2016, 35, 2, 4, 0.5, '42'),
(10, 1, 2016, 34, 2, 4, 0.5, '42'),
(11, 1, 2016, 33, 2, 4, 0.5, '42'),
(12, 1, 2016, 32, 2, 4, 0.5, '42'),
(13, 1, 2016, 38, 2, 4, 0.5, '42'),
(14, 1, 2016, 37, 2, 4, 0.5, '442'),
(15, 1, 2016, 36, 2, 4, 0.5, '42'),
(16, 1, 2016, 41, 1.5, 4, 0.375, '442'),
(17, 1, 2016, 40, 1.5, 4, 0.375, '42'),
(18, 1, 2016, 47, 2, 4, 0.5, '42'),
(19, 1, 2016, 46, 2, 4, 0.5, '2'),
(20, 1, 2016, 45, 2, 2, 1, '2'),
(21, 1, 2016, 44, 2, 2, 1, '2'),
(22, 1, 2016, 43, 2, 2, 1, '2'),
(23, 1, 2016, 49, 2, 2, 1, '2'),
(24, 1, 2016, 48, 2, 2, 1, '2'),
(25, 1, 2016, 55, 2, 2, 1, '2'),
(26, 1, 2016, 54, 4, 2, 2, '2'),
(27, 1, 2016, 53, 2, 2, 1, '2'),
(28, 1, 2016, 52, 2, 2, 1, '2'),
(29, 1, 2016, 51, 2, 2, 1, '2'),
(30, 1, 2016, 59, 2, 2, 1, '2'),
(31, 1, 2016, 58, 1, 2, 0.5, '2'),
(32, 1, 2016, 57, 1, 2, 0.5, '2'),
(33, 1, 2016, 61, 1, 2, 0.5, '2'),
(34, 1, 2016, 62, 1, 2, 0.5, '2'),
(35, 1, 2016, 65, 1, 2, 0.5, '2'),
(36, 1, 2016, 64, 2, 2, 1, '2'),
(37, 1, 2016, 1, 2, 1, 2, '1'),
(38, 1, 2016, 2, 2.5, 1, 2.5, '1'),
(39, 1, 2016, 3, 2, 1, 2, '1'),
(40, 1, 2016, 4, 2, 1, 2, '11'),
(41, 1, 2016, 5, 2, 1, 2, '1'),
(42, 1, 2016, 6, 2, 1, 2, '11'),
(43, 1, 2016, 7, 2, 1, 2, '1'),
(44, 1, 2016, 8, 2.5, 1, 2.5, '1'),
(45, 1, 2016, 9, 2, 1, 2, '1'),
(46, 1, 2016, 21, 2, 11, 0.181818, '1'),
(47, 1, 2016, 10, 2, 1, 2, '11'),
(48, 1, 2016, 11, 2, 1, 2, '1'),
(49, 1, 2016, 12, 2, 1, 2, '11'),
(50, 1, 2016, 13, 1, 1, 1, '1'),
(51, 1, 2016, 14, 1, 11, 0.0909091, '1'),
(52, 1, 2016, 15, 0.5, 1, 0.5, '1'),
(53, 1, 2016, 16, 0.5, 11, 0.0454545, '1'),
(54, 1, 2016, 17, 0.5, 1, 0.5, '1'),
(55, 1, 2016, 18, 0.5, 11, 0.0454545, '1'),
(56, 1, 2016, 19, 0.5, 1, 0.5, '1'),
(57, 1, 2016, 20, 0.5, 1, 0.5, '1');

-- --------------------------------------------------------

--
-- Table structure for table `perwakilan`
--

CREATE TABLE `perwakilan` (
  `id_perwakilan` float NOT NULL,
  `nama_perwakilan` varchar(55) NOT NULL,
  `kota_perwakilan` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perwakilan`
--

INSERT INTO `perwakilan` (`id_perwakilan`, `nama_perwakilan`, `kota_perwakilan`) VALUES
(1, 'Aceh', 'Aceh'),
(2, 'Sumatera Utara', 'Sumatera Utara'),
(3, 'Sumatera Barat', 'Sumatera Barat'),
(4, 'Riau', 'Riau'),
(5, 'Kepulauan Riau', 'Kepulauan Riau'),
(6, 'Bangka Belitung', 'Bangka Belitung'),
(7, 'Jambi', 'Jambi'),
(8, 'Bengkulu', 'Bengkulu'),
(9, 'Sumatera Selatan', 'Sumatera Selatan'),
(10, 'Lampung', 'Lampung'),
(11, 'DKI Jakarta', 'DKI Jakarta'),
(12, 'Jawa Barat', 'Jawa Barat'),
(13, 'Banten', 'Banten'),
(14, 'Jawa Tengah', 'Jawa Tengah'),
(15, 'DIY', 'DIY'),
(16, 'Jawa Timur', 'Jawa Timur'),
(17, 'Kalimantan Barat', 'Kalimantan Barat'),
(18, 'Kalimantan Tengah', 'Kalimantan Tengah'),
(19, 'Kalimantan Selatan', 'Kalimantan Selatan'),
(20, 'Kalimantan Timur', 'Kalimantan Timur'),
(21, 'Kalimantan Utara', 'Kalimantan Utara'),
(22, 'Sulawesi Utara', 'Sulawesi Utara'),
(23, 'Sulawesi Tengah', 'Sulawesi Tengah'),
(24, 'Sulawesi Tenggara', 'Sulawesi Tenggara'),
(25, 'Sulawesi Selatan', 'Sulawesi Selatan'),
(26, 'Gorontalo', 'Gorontalo'),
(27, 'Sulawesi Barat', 'Sulawesi Barat'),
(28, 'Bali', 'Bali'),
(29, 'NTB', 'NTB'),
(30, 'NTT', 'NTT'),
(31, 'Maluku', 'Maluku'),
(32, 'Maluku Utara', 'Maluku Utara'),
(33, 'Papua', 'Papua'),
(34, 'Papua Barat', 'Papua Barat');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `id_provinsi` float NOT NULL,
  `nama_provinsi` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`id_provinsi`, `nama_provinsi`) VALUES
(1, 'Aceh'),
(2, 'Sumatera Utara'),
(3, 'Sumatera Barat'),
(4, 'Riau'),
(5, 'Kepulauan Riau'),
(6, 'Bangka Belitung'),
(7, 'Jambi'),
(8, 'Bengkulu'),
(9, 'Sumatera Selatan'),
(10, 'Lampung'),
(11, 'DKI Jakarta'),
(12, 'Jawa Barat'),
(13, 'Banten'),
(14, 'Jawa Tengah'),
(15, 'DIY'),
(16, 'Jawa Timur'),
(17, 'Kalimantan Barat'),
(18, 'Kalimantan Tengah'),
(19, 'Kalimantan Selatan'),
(20, 'Kalimantan Timur'),
(21, 'Kalimantan Utara'),
(22, 'Sulawesi Utara'),
(23, 'Sulawesi Tengah'),
(24, 'Sulawesi Tenggara'),
(25, 'Sulawesi Selatan'),
(26, 'Gorontalo'),
(27, 'Sulawesi Barat'),
(28, 'Bali'),
(29, 'NTB'),
(30, 'NTT'),
(31, 'Maluku'),
(32, 'Maluku Utara'),
(33, 'Papua'),
(34, 'Papua Barat');

-- --------------------------------------------------------

--
-- Table structure for table `rsd`
--

CREATE TABLE `rsd` (
  `id_rsd` float NOT NULL,
  `nama_rsd` varchar(155) DEFAULT NULL,
  `tipe` enum('a','b','c','d') DEFAULT NULL,
  `status` enum('belum','bertahap','penuh') DEFAULT NULL,
  `id_perwakilan` varchar(11) NOT NULL,
  `id_provinsi` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rsd`
--

INSERT INTO `rsd` (`id_rsd`, `nama_rsd`, `tipe`, `status`, `id_perwakilan`, `id_provinsi`) VALUES
(1, 'RSUD Bantul', 'b', 'penuh', '15', '15'),
(4, 'rumah sakit jiwa kulon progo', 'b', 'bertahap', '', ''),
(5, 'rumah sakit amalia', 'b', 'bertahap', '2', '16'),
(6, 'RSUD Sardjito', 'a', 'penuh', '2', '16'),
(7, 'rumah sakit ibu dan anak', 'a', 'penuh', '15', '15'),
(12, 'test', 'a', 'belum', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `subaspek`
--

CREATE TABLE `subaspek` (
  `id_subaspek` float NOT NULL,
  `nama_subaspek` varchar(155) DEFAULT NULL,
  `id_aspek` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subaspek`
--

INSERT INTO `subaspek` (`id_subaspek`, `nama_subaspek`, `id_aspek`) VALUES
(1, 'Rasio Keuangan', '1'),
(2, 'Kepatuhan Pengelolaan Keuangan RSD', '1'),
(3, 'Kualitas Layanan', '2'),
(4, 'Mutu dan Manfaat Kepada Masyarakat', '2');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `level` int(2) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level`, `nickname`, `is_active`, `created`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500', 1, 'administrator', '1', '2018-01-11 09:59:20'),
(2, 'anisa', '301b96488b077ba8f9a4a01db8dab2db', 1, 'anisa rahman', '1', '0000-00-00 00:00:00'),
(3, 'Asrip', 'f2967c47e5352bd85be57a9a3a23dc3d', 1, 'Asrip', '1', '0000-00-00 00:00:00'),
(4, 'Kuwat', 'f2967c47e5352bd85be57a9a3a23dc3d', 1, 'Kuwat', '1', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aspek`
--
ALTER TABLE `aspek`
  ADD PRIMARY KEY (`id_aspek`),
  ADD KEY `id_aspek` (`id_aspek`);

--
-- Indexes for table `indikator`
--
ALTER TABLE `indikator`
  ADD PRIMARY KEY (`id_indikator`),
  ADD KEY `FK_indikator` (`id_kelindikator`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id_penilaian`);

--
-- Indexes for table `perwakilan`
--
ALTER TABLE `perwakilan`
  ADD PRIMARY KEY (`id_perwakilan`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `rsd`
--
ALTER TABLE `rsd`
  ADD PRIMARY KEY (`id_rsd`),
  ADD KEY `FK_provinsi` (`id_provinsi`),
  ADD KEY `FK_perwakilan` (`id_perwakilan`);

--
-- Indexes for table `subaspek`
--
ALTER TABLE `subaspek`
  ADD PRIMARY KEY (`id_subaspek`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aspek`
--
ALTER TABLE `aspek`
  MODIFY `id_aspek` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `indikator`
--
ALTER TABLE `indikator`
  MODIFY `id_indikator` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id_penilaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `perwakilan`
--
ALTER TABLE `perwakilan`
  MODIFY `id_perwakilan` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id_provinsi` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `rsd`
--
ALTER TABLE `rsd`
  MODIFY `id_rsd` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `subaspek`
--
ALTER TABLE `subaspek`
  MODIFY `id_subaspek` float NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
