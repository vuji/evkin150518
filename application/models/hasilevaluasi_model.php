<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Hasilevaluasi_model extends CI_Model
{

    public function getDataRasioKeu()
    {
        $query = $this->db->get('perwakilan'); //sampe ini

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataProvinsi()
    {
        $query = $this->db->get('provinsi');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataPerwakilan()
    {
        $query = $this->db->get('perwakilan');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataRSD()
    {
        $query = $this->db->get('rsd');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataAspek()
    {
        $query = $this->db->get('aspek');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataSubaspek()
    {
        $query = $this->db->get('subaspek');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataKelindikator()
    {
        $query = $this->db->get('kel_indikator');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataIndikator()
    {
        $query = $this->db->get('indikator');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getIdPerwakilan($id_perwakilan)
    {
        $this->db->from('perwakilan');
        $this->db->where('id_perwakilan', $id_perwakilan);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdProvinsi($id_provinsi)
    {
        $this->db->from('provinsi');
        $this->db->where('id_provinsi', $id_provinsi);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdRSD($id_rsd)
    {
        $this->db->from('rsd');
        $this->db->where('id_rsd', $id_rsd);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdAspek($id_aspek)
    {
        $this->db->from('aspek');
        $this->db->where('id_aspek', $id_aspek);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdSubaspek($id_subaspek)
    {
        $this->db->from('subaspek');
        $this->db->where('id_subaspek', $id_subaspek);
        $query = $this->db->get();
        return $query->row();
    }

    public function add_perwakilan($data)
    {
        $this->db->insert('perwakilan', $data);
        return $this->db->insert_id();
    }

    public function add_provinsi($data)
    {
        $this->db->insert('provinsi', $data);
        return $this->db->insert_id();
    }

    public function add_rsd($data)
    {
        $this->db->insert('rsd', $data);
        return $this->db->insert_id();
    }

    public function simpan_evaluasi($data)
    {
        return $this->db->insert('penilaian', $data);
    }

    public function update_evaluasi($condition, $data)
    {

        $this->db->where($condition);
        return $this->db->update('penilaian', $data);
    }

    public function add_aspek($data)
    {
        $this->db->insert('aspek', $data);
        return $this->db->insert_id();
    }

    public function add_subaspek($data)
    {
        $this->db->insert('subaspek', $data);
        return $this->db->insert_id();
    }

    public function update_perwakilan($where, $data)
    {
        $this->db->update('perwakilan', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_provinsi($where, $data)
    {
        $this->db->update('provinsi', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_rsd($where, $data)
    {
        $this->db->update('rsd', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_aspek($where, $data)
    {
        $this->db->update('aspek', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_subaspek($where, $data)
    {
        $this->db->update('subaspek', $data, $where);
        return $this->db->affected_rows();
    }

    public function del_perwakilan($id_perwakilan)
    {
        $this->db->where('id_perwakilan', $id_perwakilan);
        $this->db->delete('perwakilan');
    }

    public function del_provinsi($id_provinsi)
    {
        $this->db->where('id_provinsi', $id_provinsi);
        $this->db->delete('provinsi');
    }

    public function del_rsd($id_rsd)
    {
        $this->db->where('id_rsd', $id_rsd);
        $this->db->delete('rsd');
    }

    public function del_aspek($id_aspek)
    {
        $this->db->where('id_aspek', $id_aspek);
        $this->db->delete('aspek');
    }

    public function del_subaspek($id_subaspek)
    {
        $this->db->where('id_subaspek', $id_subaspek);
        $this->db->delete('subaspek');
    }

    public function getDataJenisSK()
    {
        $this->db->join('sk_grup sg', 'sg.id_grupsk = sj.id_grupsk');
        $query = $this->db->get('sk_jenis sj');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getIDJenisSK($id_jenissk)
    {
        $this->db->from('sk_jenis');
        $this->db->where('id_jenissk', $id_jenissk);
        $query = $this->db->get();
        return $query->row();
    }

    public function add_jenissk($data)
    {
        $this->db->insert('sk_jenis', $data);
        return $this->db->insert_id();
    }

    public function update_jenissk($where, $data)
    {
        $this->db->update('sk_jenis', $data, $where);
        return $this->db->affected_rows();
    }

    public function del_jenissk($id_jenissk)
    {
        $this->db->where('id_jenissk', $id_jenissk);
        $this->db->delete('sk_jenis');
    }

    public function getDataUser()
    {
        $this->db->where('is_active', '1');
        $query = $this->db->get('user');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getByIDUser($id_user)
    {
        return $this->db->get_where('user', array('id_user' => $id_user))->row();
    }

    public function addUser()
    {

        $data = array(
            'id_user' => $this->input->post('id_user'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'level' => $this->input->post('level'),
            'nickname' => $this->input->post('nickname'),
            'is_active' => '1',
            'created' => 'NOW()',
        );
        $this->db->insert('user', $data);
    }

    public function updateUser($username)
    {
        $id_user = $this->input->post('id_user');
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $level = $this->input->post('level');
        $nickname = $this->input->post('nickname');
        $data = array(
            'id_user' => $id_user,
            'username' => $username,
            'password' => $password,
            'level' => $level,
            'nickname' => $nickname,
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);
    }

    public function deleteUser($id_user)
    {
        $data = array(
            'is_active' => '0',
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);
    }
    public function getAllDataPeg($data)
    {
        $select = '';
        if (!empty($data)) {
            for ($i = 0; $i < count($data); $i++) {
                $select .= $data[$i];
                if ($i != (count($data) - 1)) {
                    $select .= ',';
                }
            }
        } else {
            $select = '*';
        }

        $this->db->select($select);
        $this->db->where('is_active', '1');
        $query = $this->db->get('datapeg');
        return $query->result();
    }

    public function getPegawaiById($nip)
    {
        return $this->db->get_where('datapeg', array('nip_gabungan' => $nip))->row();
    }

    public function data_keuangan()
    {

        return $this->db->query("SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,SUM(IF(aspek.id_aspek=1,capaian,0)) As capaian_keuangan,
       SUM(IF(aspek.id_aspek=2,capaian,0)) As capaian_layanan,
        SUM(IF(aspek.id_aspek=1,nilai,0)) As nilai_keuangan,
         SUM(IF(aspek.id_aspek=2,nilai,0)) As nilai_layanan,
          SUM(IF(aspek.id_aspek=1,persentase,0)) As persentase_keuangan,
         SUM(IF(aspek.id_aspek=2,persentase,0)) As persentase_layanan FROM indikator
		    join penilaian on indikator.id_indikator = penilaian.id_indikator
		    JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    join subaspek on subaspek.id_subaspek = indikator.id_subaspek
		    join aspek on aspek.id_aspek = subaspek.id_aspek
		    GROUP by tahun,penilaian.id_rsd")->result();
    }

    public function data_pelayanan()
    {

        return $this->db->query("SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,sum(nilai) as nilai,sum(capaian) as capaian,sum(persentase) as persentase FROM indikator  as parent_indikator
            join indikator as child_indikator on parent_indikator .id_indikator = child_indikator.id_kelindikator
		    join penilaian on child_indikator.id_indikator = penilaian.id_indikator
		    JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    join subaspek on subaspek.id_subaspek = parent_indikator.id_subaspek
		    join aspek on aspek.id_aspek = subaspek.id_aspek
		    where aspek.id_aspek = 2 and parent_indikator.bobot = 0
		    GROUP by tahun,penilaian.id_rsd")->result();
    }

    public function data_keuangan_by_rsd_tahun($id_rsd, $tahun)
    {
        return $this->db->query("SELECT tahun,penilaian.id_indikator as id_indikator,nama_subaspek,nama_rsd,penilaian.id_rsd,persentase,nilai,capaian,penyebab,nama_indikator,bobot FROM indikator
			join penilaian on indikator.id_indikator = penilaian.id_indikator
		    JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    join subaspek on subaspek.id_subaspek = indikator.id_subaspek
		    join aspek on aspek.id_aspek = subaspek.id_aspek
			WHERE aspek.id_aspek = 1 AND penilaian.id_rsd = $id_rsd AND penilaian.tahun = $tahun")->result();

    }

    public function data_pelayanan_by_rsd_tahun($id_rsd, $tahun)
    {

        return $this->db->query("SELECT tahun,nama_subaspek,nama_rsd,penilaian.id_rsd as id_rsd,nilai,capaian,persentase,penyebab, parent_indikator.id_indikator as id_parent_indikator,child_indikator.id_indikator as id_child_indikator, parent_indikator.nama_indikator as indikator_parent_name,
			child_indikator.nama_indikator as indikator_child_name,
			child_indikator.bobot as bobot
			FROM indikator as parent_indikator
			join indikator as child_indikator
			on parent_indikator.id_indikator = child_indikator.id_kelindikator
			join penilaian on child_indikator.id_indikator = penilaian.id_indikator
			JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    join subaspek on subaspek.id_subaspek = parent_indikator.id_subaspek
		    join aspek on aspek.id_aspek = subaspek.id_aspek
		    where aspek.id_aspek = 2 and parent_indikator.bobot = 0
		    AND penilaian.id_rsd = $id_rsd AND penilaian.tahun = $tahun
		    order by parent_indikator.id_indikator,parent_indikator.id_indikator ASC")->result();
    }

    public function getRSDbyperwakilan($id_rsd)
    {
        $hasil = $this->db->query("SELECT * FROM rsd WHERE id_perwakilan = (SELECT id_perwakilan FROM rsd WHERE id_rsd = $id_rsd)");
        return $hasil->result();
    }
}
