<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Report_model extends CI_Model
{

    public function laporan_kompilasi($where = null)
    {
        $wherestr = array();
        foreach ($where as $key => $val) {
            if ($val != null) {
                $wherestr[] = $key . " = " . " '" . $val . "' ";

            }
        }

        $conditions = null;

        if ($wherestr != null) {
            $conditions = ($where != '' and count($where) >= 1) ? " WHERE " . implode(" AND ", $wherestr) : '';
        }

        return $this->db->query("SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,tipe,
SUM(IF(aspek.id_aspek=1,capaian,0)) As capaian_keuangan,
       SUM(IF(aspek.id_aspek=2,capaian,0)) As capaian_layanan,
        SUM(IF(aspek.id_aspek=1,nilai,0)) As nilai_keuangan,
         SUM(IF(aspek.id_aspek=2,nilai,0)) As nilai_layanan
FROM indikator
		     join penilaian on indikator.id_indikator = penilaian.id_indikator
		    left  JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    left  join subaspek on subaspek.id_subaspek = indikator.id_subaspek
		     left join aspek on aspek.id_aspek = subaspek.id_aspek

             {$conditions}

             GROUP by tahun,penilaian.id_rsd")->result();
    }

    public function getDataPerwakilan()
    {
        $query = $this->db->get('perwakilan');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataProvinsi()
    {
        $query = $this->db->get('provinsi');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataRSD()
    {
        $query = $this->db->get('rsd');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function tipeRSD($tipe)
    {
        $q = "SELECT count(*) AS jumlah FROM rsd WHERE tipe = '$tipe' GROUP BY tipe";
        $query = $this->db->query($q);
        return $query->result();
    }

    public function statusRSD($status)
    {
        $q = "SELECT count(*) AS jumlah FROM rsd WHERE status = '$status' GROUP BY status";
        $query = $this->db->query($q);
        return $query->result();
    }

    public function getData()
    {
        $perwakilan = $this->input->post('$id_perwakilan');
        $provinsi = $this->input->post('id_provinsi');
        $rsd = $this->input->post('id_rsd');
        $tahun = $this->input->post('tahun');

        $q = "SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,sum(nilai) as nilai,sum(capaian) as capaian,sum(persentase) as persentase FROM indikator
		    join penilaian on indikator.id_indikator = penilaian.id_indikator
		    JOIN rsd on rsd.id_rsd = penilaian.id_rsd
		    join subaspek on subaspek.id_subaspek = indikator.id_subaspek
		    join aspek on aspek.id_aspek = subaspek.id_aspek
		    where aspek.id_aspek = 1
		    GROUP by tahun,penilaian.id_rsd";
        $query = $this->db->query($q);
        return $query->result();

    }

    public function getDataRSDByPerw($id_perwakilan)
    {
        $this->db->select('id_rsd, nama_rsd');
        $this->db->from('rsd');
        $this->db->where('id_perwakilan', $id_perwakilan);
        $this->db->order_by('nama_rsd', 'asc');
        return $this->db->get()->result();
    }

}
