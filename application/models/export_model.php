<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Export_model extends CI_Model {
    // get employee list
    public function exportExcel() {

        return $this->db->query("SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,tipe, sum(nilai) as nilai,sum(capaian) as capaian FROM indikator
            join penilaian on indikator.id_indikator = penilaian.id_indikator
            JOIN rsd on rsd.id_rsd = penilaian.id_rsd
            join subaspek on subaspek.id_subaspek = indikator.id_subaspek
            join aspek on aspek.id_aspek = subaspek.id_aspek
            where aspek.id_aspek = 1
            GROUP by tahun,penilaian.id_rsd")->result();
    }

    public function search()
        {
            $id_perwakilan = $this->input->post('id_perwakilan');
            //$id_provinsi = $this->input->post('id_provinsi');
            //$id_rsd = $this->input->post('id_rsd');
            $tahun = $this->input->post('tahun');

            // echo "bulan : $bulan";
            // echo "tahun : $tahun";
            //die();

            $this->session->set_userdata('id_perwakilan',$id_perwakilan);  
            //$this->session->set_userdata('id_provinsi',$id_provinsi);
            //$this->session->set_userdata('id_rsd',$id_rsd);
            $this->session->set_userdata('tahun',$tahun);

            if(empty($id_perwakilan) && empty($tahun)) {
                $conditon = 'and';
            }elseif(!empty($id_perwakilan) && !empty($tahun)) {
               $conditon = 'and';
            }else {

                $conditon = 'or';
            }

            $sql="SELECT tahun,penilaian.id_rsd as id_rsd,nama_rsd,tipe, sum(nilai) as nilai,sum(capaian) as capaian FROM indikator
            join penilaian on indikator.id_indikator = penilaian.id_indikator
            JOIN rsd on rsd.id_rsd = penilaian.id_rsd
            join subaspek on subaspek.id_subaspek = indikator.id_subaspek
            join aspek on aspek.id_aspek = subaspek.id_aspek
            where aspek.id_aspek = 1
            GROUP by tahun,penilaian.id_rsd";
            //echo $sql;

            $query=$this->db->query($sql);
            // echo $bulan;
            // echo $tahun;
            // die();

            if ($query->num_rows() > 0){
                return $query->result_array();
            }
        
        }
}
?>