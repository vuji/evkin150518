<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_model extends CI_Model
{

    public function getDataUser()
    {
        $this->db->where('is_active', '1');
        $query = $this->db->get('user');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getByIDUser($id_user)
    {
        return $this->db->get_where('user', array('id_user' => $id_user))->row();
    }

    public function addUser()
    {

        $data = array(
            'id_user' => $this->input->post('id_user'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'level' => $this->input->post('level'),
            'nickname' => $this->input->post('nickname'),
            'is_active' => '1',
            'created' => 'NOW()',
        );
        $this->db->insert('user', $data);
    }

    public function updateUser($username)
    {
        $id_user = $this->input->post('id_user');
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $level = $this->input->post('level');
        $nickname = $this->input->post('nickname');
        $data = array(
            'id_user' => $id_user,
            'username' => $username,
            'password' => $password,
            'level' => $level,
            'nickname' => $nickname,
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);
    }

    public function deleteUser($id_user)
    {
        $data = array(
            'is_active' => '0',
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);
    }

    public function getDataPerwakilan()
    {
        $query = $this->db->get('perwakilan');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataProvinsi()
    {
        $query = $this->db->get('provinsi');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataRSD()
    {
        $query = $this->db->get('rsd');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataRSDByIdPerwakilan($id)
    {

        $hasil = array();
        $this->db->where('id_perwakilan', $id);
        $this->db->select('id_rsd,nama_rsd');
        $query = $this->db->get('rsd');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[$data->id_rsd] = $data->nama_rsd;
            }
            // return $hasil;
        }

        echo json_encode($hasil);

    }

    public function getDataAspek()
    {
        $query = $this->db->get('aspek');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataSubaspek()
    {
        $this->db->join('aspek', 'aspek.id_aspek=subaspek.id_aspek');

        $query = $this->db->get('subaspek');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataKelindikator()
    {
        $this->db->join('subaspek', 'subaspek.id_subaspek=indikator.id_subaspek');
        $this->db->join('aspek', 'aspek.id_aspek=subaspek.id_aspek');
        $this->db->where('bobot', 0);

        $query = $this->db->get('indikator');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataIndikator($id_aspek = '')
    {
        $this->db->join('subaspek', 'subaspek.id_subaspek=indikator.id_subaspek');
        $this->db->join('aspek', 'aspek.id_aspek=subaspek.id_aspek');

        if ($id_aspek != '') {
            $this->db->where('aspek.id_aspek', $id_aspek);
        }

        $query = $this->db->get('indikator');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getDataIndikatorAspekPelayanan()
    {
        return $this->db->query("SELECT nama_subaspek, parent_indikator.id_indikator as id_parent_indikator,child_indikator.id_indikator as id_child_indikator, parent_indikator.nama_indikator as indikator_parent_name,
			child_indikator.nama_indikator as indikator_child_name,
			child_indikator.bobot as bobot
			FROM indikator as parent_indikator
			join indikator as child_indikator
			on parent_indikator.id_indikator = child_indikator.id_kelindikator
			join subaspek on parent_indikator.id_subaspek = subaspek.id_subaspek
			WHERE parent_indikator.bobot = 0
			order by parent_indikator.id_indikator,parent_indikator.id_indikator ASC")->result();
    }

    //get row data

    public function getIdPerwakilan($id_perwakilan)
    {
        $this->db->from('perwakilan');
        $this->db->where('id_perwakilan', $id_perwakilan);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdProvinsi($id_provinsi)
    {
        $this->db->from('provinsi');
        $this->db->where('id_provinsi', $id_provinsi);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdRSD($id_rsd)
    {
        $this->db->from('rsd');
        $this->db->where('id_rsd', $id_rsd);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdAspek($id_aspek)
    {
        $this->db->from('aspek');
        $this->db->where('id_aspek', $id_aspek);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdSubaspek($id_subaspek)
    {
        $this->db->from('subaspek');
        $this->db->where('id_subaspek', $id_subaspek);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdKelindikator($id_kelindikator)
    {
        $this->db->from('kel_indikator');
        $this->db->where('id_kelindikator', $id_kelindikator);
        $query = $this->db->get();
        return $query->row();
    }

    public function getIdIndikator($id_indikator)
    {
        $this->db->from('indikator');
        $this->db->where('id_indikator', $id_indikator);
        $query = $this->db->get();
        return $query->row();
    }

    //add
    public function add_perwakilan($data)
    {
        $this->db->insert('perwakilan', $data);
        return $this->db->insert_id();
    }

    public function add_provinsi($data)
    {
        // print_r($data);
        // die();
        $this->db->insert('provinsi', $data);
        return $this->db->insert_id();

    }

    public function add_rsd($data)
    {
        $this->db->insert('rsd', $data);
        return $this->db->insert_id();
    }

    public function add_aspek($data)
    {
        $this->db->insert('aspek', $data);
        return $this->db->insert_id();
    }

    public function add_subaspek($data)
    {
        $this->db->insert('subaspek', $data);
        return $this->db->insert_id();
    }

    public function add_kelindikator($data)
    {
        $this->db->insert('kel_indikator', $data);
        return $this->db->insert_id();
    }

    public function add_indikator($data)
    {
        $this->db->insert('indikator', $data);
        return $this->db->insert_id();
    }

    //update

    public function update_perwakilan($where, $data)
    {
        $this->db->update('perwakilan', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_provinsi($where, $data)
    {
        $this->db->update('provinsi', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_rsd($where, $data)
    {
        $this->db->update('rsd', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_aspek($where, $data)
    {
        $this->db->update('aspek', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_subaspek($where, $data)
    {
        $this->db->update('subaspek', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_kelindikator($where, $data)
    {
        $this->db->update('kel_indikator', $data, $where);
        return $this->db->affected_rows();
    }

    public function update_indikator($where, $data)
    {
        $this->db->update('indikator', $data, $where);
        return $this->db->affected_rows();
    }

    //delete

    public function del_perwakilan($id_perwakilan)
    {
        $this->db->where('id_perwakilan', $id_perwakilan);
        $this->db->delete('perwakilan');
    }

    public function del_provinsi($id_provinsi)
    {
        $this->db->where('id_provinsi', $id_provinsi);
        $this->db->delete('provinsi');
    }

    public function del_rsd($id_rsd)
    {
        $this->db->where('id_rsd', $id_rsd);
        $this->db->delete('rsd');
    }

    public function del_aspek($id_aspek)
    {
        $this->db->where('id_aspek', $id_aspek);
        $this->db->delete('aspek');
    }

    public function del_subaspek($id_subaspek)
    {
        $this->db->where('id_subaspek', $id_subaspek);
        $this->db->delete('subaspek');
    }

    public function del_kelindikator($id_kelindikator)
    {
        $this->db->where('id_kelindikator', $id_kelindikator);
        $this->db->delete('kel_indikator');
    }

    public function del_indikator($id_indikator)
    {
        $this->db->where('id_indikator', $id_indikator);
        $this->db->delete('indikator');
    }

    //get data refrence by id

}
