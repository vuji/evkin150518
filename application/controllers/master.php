<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('master_model'));
    }

    public function index()
    {
        show_404();
    }

    //punya simoka
    public function datapegawai()
    {
        $data['title'] = 'Master - Data Pegawai';
        $data['result'] = $this->master_model->getDataPeg();

        $data['main_content'] = 'master/datapeg';
        $this->load->view('template/main', $data);
    }

    //preferensi perwakilan
    public function perwakilan()
    {
        $data['title'] = 'Siekin - Perwakilan';
        $data['result'] = $this->master_model->getDataPerwakilan();

        // $result = $this->master_model->getDataPerwakilan();
        // print_r($result);
        // die();

        $data['main_content'] = 'master/perwakilan';
        $this->load->view('template/main', $data);
    }

    public function provinsi()
    {
        $data['title'] = 'Siekin - Provinsi';
        $data['result'] = $this->master_model->getDataProvinsi();

        $data['main_content'] = 'master/provinsi';
        $this->load->view('template/main', $data);
    }

    public function rsd()
    {
        $data['title'] = 'Siekin - Rumah Sakit';
        $data['result'] = $this->master_model->getDataRSD();
        $data['perwakilan'] = $this->master_model->getDataPerwakilan();
        $data['provinsi'] = $this->master_model->getDataProvinsi();

        $data['main_content'] = 'master/rsd';
        $this->load->view('template/main', $data);
    }

    public function rsd_lists($id_perwakilan)
    {
        $this->master_model->getDataRSDByIdPerwakilan($id_perwakilan);
    }

    public function aspek()
    {
        $data['title'] = 'Siekin - Aspek';
        $data['result'] = $this->master_model->getDataAspek();

        $data['main_content'] = 'master/aspek';
        $this->load->view('template/main', $data);
    }

    public function subaspek()
    {
        $data['title'] = 'Siekin - Sub Aspek';
        $data['result'] = $this->master_model->getDataSubaspek();
        $data['aspek'] = $this->master_model->getDataAspek();

        $data['main_content'] = 'master/subaspek';
        $this->load->view('template/main', $data);
    }

    // function kelindikator()
    // {
    //     $data['title'] = 'Siekin - Kelompok Indikator';
    //     $data['result'] = $this->master_model->getDataKelIndikator();
    //     $data['subaspek'] = $this->master_model->getDataSubaspek();

    //        $data['main_content'] = 'master/kelindikator';
    //     $this->load->view('template/main',$data);
    // }

    public function indikator()
    {
        $data['title'] = 'Siekin - Indikator';
        $data['resultkeuangan'] = $this->master_model->getDataIndikator(1);
        $data['resultpelayanan'] = $this->master_model->getDataIndikatorAspekPelayanan();
        $data['subaspek'] = $this->master_model->getDataSubaspek();
        $data['kelindikator'] = $this->master_model->getDataKelIndikator();

        $data['main_content'] = 'master/indikator';
        $this->load->view('template/main', $data);
    }

    //get id perwakilan
    public function getIdPerwakilan($id_perwakilan)
    {
        $data = $this->master_model->getIdPerwakilan($id_perwakilan);
        echo json_encode($data);
    }

    public function getIdProvinsi($id_provinsi)
    {
        $data = $this->master_model->getIdProvinsi($id_provinsi);
        echo json_encode($data);
    }

    public function getIdRSD($id_rsd)
    {
        $data = $this->master_model->getIdRSD($id_rsd);
        echo json_encode($data);
    }

    public function getIdAspek($id_aspek)
    {
        $data = $this->master_model->getIdAspek($id_aspek);
        echo json_encode($data);
    }

    public function getIdSubaspek($id_subaspek)
    {
        $data = $this->master_model->getIdSubaspek($id_subaspek);
        echo json_encode($data);
    }

    public function getIdKelindikator($id_kelindikator)
    {
        $data = $this->master_model->getIdKelindikator($id_kelindikator);
        echo json_encode($data);
    }

    public function getIdIndikator($id_indikator)
    {
        $data = $this->master_model->getIdIndikator($id_indikator);
        echo json_encode($data);
    }

    public function add_perwakilan()
    {
        $data = array(
            'id_perwakilan' => $this->input->post('id_perwakilan'),
            'nama_perwakilan' => $this->input->post('nama_perwakilan'),
            'kota_perwakilan' => $this->input->post('kota_perwakilan'),
        );
        $insert = $this->master_model->add_perwakilan($data);

        // print_r($insert);
        // die();

        echo json_encode(array("status" => true));
    }

    public function add_provinsi()
    {
        $data = array(
            'id_provinsi' => $this->input->post('id_provinsi'),
            'nama_provinsi' => $this->input->post('nama_provinsi'),
        );
        $insert = $this->master_model->add_provinsi($data);

        echo json_encode(array(
            "status" => true,
            "message" => '<div class="alert alert-success"><strong>Success!</strong> data berhasil di simpan</div>',
        ));
    }

    public function add_rsd()
    {
        $data = array(
            'nama_rsd' => $this->input->post('nama_rsd'),
            'pemilik' => $this->input->post('pemilik'),
            'tipe' => $this->input->post('tipe'),
            'status' => $this->input->post('status'),
            'id_perwakilan' => $this->input->post('id_perwakilan'),
            'id_provinsi' => $this->input->post('id_provinsi'),

        );
        $insert = $this->master_model->add_rsd($data);
        echo json_encode(array(
            "status" => true,
            "message" => '<div class="alert alert-success"><strong>Success!</strong> data berhasil di simpan</div>',
        ));
    }

    public function add_aspek()
    {
        $data = array(
            'nama_aspek' => $this->input->post('nama_aspek'),
        );
        $insert = $this->master_model->add_aspek($data);
        echo json_encode(array("status" => true));
    }

    public function add_subaspek()
    {
        $data = array(

            'nama_subaspek' => $this->input->post('nama_subaspek'),
            'id_aspek' => $this->input->post('id_aspek'),
        );
        $insert = $this->master_model->add_subaspek($data);
        echo json_encode(array("status" => true));
    }

    public function add_indikator()
    {

        $data = array(
            'id_subaspek' => $this->input->post('id_subaspek'),
            'id_kelindikator' => $this->input->post('id_kelindikator'),
            'nama_indikator' => $this->input->post('nama_indikator'),
            'bobot' => $this->input->post('bobot'),

        );
        $insert = $this->master_model->add_indikator($data);

        echo json_encode(array("status" => true));
    }

    public function update_perwakilan()
    {
        $data = array(
            'nama_perwakilan' => $this->input->post('nama_perwakilan'),
            'kota_perwakilan' => $this->input->post('kota_perwakilan'),
        );
        $this->master_model->update_perwakilan(array('id_perwakilan' => $this->input->post('id_perwakilan')), $data);
        echo json_encode(array("status" => true));
    }

    public function update_provinsi()
    {
        $data = array(
            'nama_provinsi' => $this->input->post('nama_provinsi'),
        );
        $this->master_model->update_provinsi(array('id_provinsi' => $this->input->post('id_provinsi')), $data);
        echo json_encode(array("status" => true));
    }

    public function update_rsd()
    {
        $data = array(
            'nama_rsd' => $this->input->post('nama_rsd'),
            'pemilik' => $this->input->post('pemilik'),
            'tipe' => $this->input->post('tipe'),
            'status' => $this->input->post('status'),
            'id_perwakilan' => $this->input->post('id_perwakilan'),
            'id_provinsi' => $this->input->post('id_provinsi'),
        );
        $this->master_model->update_rsd(array('id_rsd' => $this->input->post('id_rsd')), $data);
        echo json_encode(array(
            "status" => true,
            "message" => '<div class="alert alert-success"><strong>Success!</strong> data berhasil di ubah</div>',
        ));
    }

    public function update_aspek()
    {
        $data = array(
            'nama_aspek' => $this->input->post('nama_aspek'),
        );
        $this->master_model->update_aspek(array('id_aspek' => $this->input->post('id_aspek')), $data);
        echo json_encode(array("status" => true));
    }

    public function update_subaspek()
    {
        $data = array(
            'nama_aspek' => $this->input->post('nama_aspek'),
            'id_aspek' => $this->input->post('id_aspek'),
        );
        $this->master_model->update_subaspek(array('id_subaspek' => $this->input->post('id_subaspek')), $data);
        echo json_encode(array("status" => true));
    }

    public function update_kelindikator()
    {
        $data = array(
            'nama_kelindikator' => $this->input->post('nama_kelindikator'),
            'id_subaspek' => $this->input->post('id_subaspek'),
        );
        $this->master_model->update_kelindikator(array('id_kelindikator' => $this->input->post('id_kelindikator')), $data);
        echo json_encode(array("status" => true));
    }

    public function update_indikator()
    {
        $data = array(
            'nama_indikator' => $this->input->post('nama_indikator'),
            'bobot' => $this->input->post('bobot'),
            'id_kelindikator' => $this->input->post('id_kelindikator'),
        );
        $this->master_model->update_indikator(array('id_indikator' => $this->input->post('id_indikator')), $data);
        echo json_encode(array("status" => true));
    }

    public function del_perwakilan($id_perwakilan)
    {
        $this->master_model->del_perwakilan($id_perwakilan);
        echo json_encode(array("status" => true));
    }

    public function del_provinsi($id_provinsi)
    {
        $this->master_model->del_provinsi($id_provinsi);
        echo json_encode(array("status" => true));
    }

    public function del_rsd($id_rsd)
    {
        $this->master_model->del_rsd($id_rsd);
        echo json_encode(array(
            "status" => true,
            "message" => '<div class="alert alert-success"><strong>Success!</strong> data berhasil di hapus</div>',
        ));
    }

    public function del_aspek($id_aspek)
    {
        $this->master_model->del_aspek($id_aspek);
        echo json_encode(array("status" => true));
    }

    public function del_subaspek($id_subaspek)
    {
        $this->master_model->del_subaspek($id_subaspek);
        echo json_encode(array("status" => true));
    }

    public function del_kelindikator($id_kelindikator)
    {
        $this->master_model->del_kelindikator($id_kelindikator);
        echo json_encode(array("status" => true));
    }

    public function del_indikator($id_indikator)
    {
        $this->master_model->del_indikator($id_indikator);
        echo json_encode(array("status" => true));
    }

}
