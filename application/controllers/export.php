<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Export extends CI_Controller
{
    // construct
    public $nama_tabel = 'penilaian';
    public function __construct()
    {
        parent::__construct();
        // load model
        $this->load->library('excel');
        $this->load->model('export_model', 'export');
        $this->load->model(array('report_model'));
        $this->load->model(array('hasilevaluasi_model'));

    }
    // export xlsx|xls file
    public function index()
    {
        $data['page'] = 'export-excel';
        $data['title'] = 'Export Excel data ' . time();
        // $data['exportExcel'] = $this->export->exportExcel();

        $condition['id_provinsi'] = isset($_GET['id_provinsi']) ? $_GET['id_provinsi'] : '';
        $condition['id_perwakilan'] = isset($_GET['id_perwakilan']) ? $_GET['id_perwakilan'] : '';
        $condition['penilaian.id_rsd'] = isset($_GET['id_rsd']) ? $_GET['id_rsd'] : '';
        $condition['tahun'] = isset($_GET['tahun']) ? $_GET['tahun'] : '';
        $data['result'] = $this->report_model->laporan_kompilasi($condition);

        // load view file for output
        $this->load->view('report/export_els', $data);
    }

    public function exl_rsd_item($id_rsd, $tahun)
    {
        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['result'] = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($id_rsd, $tahun);
        $data['resultpelayanan'] = $this->hasilevaluasi_model->data_pelayanan_by_rsd_tahun($id_rsd, $tahun);
        $data['total_bobot'] = 0;
        $data['total_skor'] = 0;

        // $data['main_content'] = 'report/detail_els_rsd';

        $this->load->view('report/detail_els_rsd', $data);
    }

    // // create xlsx
    // public function createXLS()
    // {
    //     // create file name
    //     $fileName = 'laporan-' . time() . '.xlsx';
    //     // load excel library
    //     $this->load->library('excel');
    //     $exportExcel = $this->export->exportExcel();
    //     $objPHPExcel = new PHPExcel();
    //     $objPHPExcel->setActiveSheetIndex(0);
    //     // set Header
    //     $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'No');
    //     $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nama RSD');
    //     $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Tahun');
    //     $objPHPExcel->getActiveSheet()->SetCellVfalue('D1', 'Total Skor Aspek Keuangan');
    //     $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total Skor Aspek Pelayanan');
    //     $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Jumlah Total Skor');
    //     $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Kategori');
    //     // set Row
    //     $rowCount = 2;
    //     foreach ($exportExcel as $element) {
    //         $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['no']);
    //         $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['nama_rsd']);
    //         $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['tahun']);
    //         $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['dob']);
    //         $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['contact_no']);
    //         $rowCount++;
    //     }
    //     $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    //     $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
    //     // download file
    //     header("Content-Type: application/vnd.ms-excel");
    //     redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
    // }

    // public function excel()
    // {
    //     $this->load->model('export_model');
    //     $data['result'] = $this->export_model->search();
    //     $this->load->view('excel', $data);

    // }

    // public function export()
    // {
    //     //membuat objek
    //     $this->load->model('export_model');
    //     $objPHPExcel = new PHPExcel();
    //     $data = $this->db->get($this->export_model->search());

    //     // Nama Field Baris Pertama
    //     $fields = $data->list_fields();
    //     $col = 0;
    //     foreach ($fields as $field) {
    //         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
    //         $col++;
    //     }

    //     // Mengambil Data
    //     $row = 2;
    //     foreach ($data->result() as $data) {
    //         $col = 0;
    //         foreach ($fields as $field) {
    //             $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
    //             $col++;
    //         }

    //         $row++;
    //     }
    //     $objPHPExcel->setActiveSheetIndex(0);

    //     //Set Title
    //     $objPHPExcel->getActiveSheet()->setTitle('Laporan Evaluasi Kinerja');

    //     //Save ke .xlsx, kalau ingin .xls, ubah 'Excel2007' menjadi 'Excel5'
    //     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    //     //Header
    //     header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    //     header("Cache-Control: no-store, no-cache, must-revalidate");
    //     header("Cache-Control: post-check=0, pre-check=0", false);
    //     header("Pragma: no-cache");
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    //     //Nama File
    //     header('Content-Disposition: attachment;filename="laporan_evkin_blud.xlsx"');

    //     //Download
    //     $objWriter->save("php://output");

    // }

}
