<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('report_model'));
        $this->load->model(array('master_model'));

    }

    public function laporan_kompilasi()
    {

        $data['title'] = 'Laporan Hasil Evaluasi Kinerja RSD-BLUD';
        $condition['id_provinsi'] = isset($_GET['id_provinsi']) ? $_GET['id_provinsi'] : '';
        $condition['id_perwakilan'] = isset($_GET['id_perwakilan']) ? $_GET['id_perwakilan'] : '';
        $condition['penilaian.id_rsd'] = isset($_GET['id_rsd']) ? $_GET['id_rsd'] : '';
        $condition['tahun'] = isset($_GET['tahun']) ? $_GET['tahun'] : '';
        $data['result'] = $this->report_model->laporan_kompilasi($condition);
        // $result = $this->report_model->laporan_kompilasi();
        // print_r($result);
        // die();

        $data['rsd'] = $this->report_model->getDataRSD();
        $data['provinsi'] = $this->report_model->getDataProvinsi();
        $data['perwakilan'] = $this->report_model->getDataPerwakilan();
        // $data['rsdByPerw'] = $this->report_model->getDataRSDByPerw($id_perwakilan);

        // $data['result'] = $this->report_model->getData();

        // $term = array();
        // $data['perwakilan'] = 0;
        // $data['rsd'] = 0;
        // $data['data_zona'] = array();

        // if($_POST){
        //     if(!empty($_POST['json_data_laporan'])){
        //         $data_json = $POST['json_data_laporan'];
        //         $this->export($data_json);
        //     }

        //     $term = array(
        //         'id_provinsi' => $this->input->post('id_provinsi'),
        //         'tahun' => $this->input->post('tahun'));

        //     $data['perwakilan'] = $this->input->post('id_perwakilan');
        //     if($this->input->post('id_rsd')!=''){
        //         $data['rsd'] = $this->input->post('id_rsd');
        //         $data['data_zona'] = $this->report_model-> getDataZona($this->input->post('id_rsd'));
        //     }
        //     $data['result'] = $this->report_model->getData();

        // }

        // $data['term'] = $term;

        $data['main_content'] = 'report/report';
        $this->load->view('template/main', $data);

    }

    public function rsdByPerw()
    {
        $id_perwakilan = $this->input->post('id_perwakilan');
        $rsd = $this->report_model->getDataRSDByPerw($id_perwakilan);
        $default_rsdByPerw = $this->input->post('default_rsdByPerw');
        if (count($rsd) == 0) {
            echo 0;
        } else {
            echo '<option>--Pilih--</option>';
            foreach ($rsd as $k) {
                $selected = ($default_rsdByPerw == $k->id_rsd) ? "selected" : "";
                echo '<option value="' . $k->id_rsd . '" ' . $selected . '>' . $k->nama_rsd . '</option>';
            }
        }
    }

}
