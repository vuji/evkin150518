<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){  
	  parent::__construct();
	    $this->load->model(array('master_model'));
	}

	function index()
	{
		$data['title'] = 'User';
		$data['result'] = $this->master_model->getDataUser();
        $data['main_content'] = 'user/user';

		$this->load->view('template/main',$data);
	}

	function add_user()
	{
		$data['title'] = 'User - Tambah User';
        $data['main_content'] = 'user/user_add';

        $username = $this->input->post('username');
		$this->form_validation->set_rules('id_user','ID User','');
		$this->form_validation->set_rules('username','Username','trim|required|min_length[4]');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2','Konfirmasi Password','trim|required|matches[password]');
		$this->form_validation->set_rules('nickname','Nickname','');
		$this->form_validation->set_rules('level','Hak Akses','required');
		
		if ($this->form_validation->run()==FALSE) {
			$this->load->view('template/main', $data);
		} else {
			$this->master_model->addUser();
			redirect('user');
		}
	}

	function update_user($id_user)
	{
		$data['title'] = 'Master - Ubah Data User';
        $data['main_content'] = 'user/user_update';

        $username = $this->input->post('username');
		$this->form_validation->set_rules('id_user','ID User','');
		$this->form_validation->set_rules('username','Username','trim|required|min_length[4]');
		$this->form_validation->set_rules('password','Password','trim|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2','Konfirmasi Password','trim|matches[password]');
		$this->form_validation->set_rules('nickname','Nickname','');
		
		if ($this->form_validation->run()==FALSE) {
			$data['result'] = new stdClass();
			if ($this->input->post()!=null){
				foreach ($this->input->post() as $key => $value) {
					$data['result']->$key = $value;
				}
			} else {
				$data['result'] = $this->master_model->getByIDUser($id_user);
			}
			$this->load->view('template/main', $data);
		} else {
			$this->master_model->updateUser();
			redirect('user');
		}
	}

	function delete_user($id_user)
	{
		$this->master_model->deleteUser($id_user);
		$this->session->set_flashdata('notif', "Data berhasil dihapus");
		redirect('user');
	}
}