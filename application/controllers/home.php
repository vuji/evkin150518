<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){  
	  parent::__construct();
	    $this->load->model(array('report_model'));
            
	}


	
	public function index()
	{
		$data['title'] = 'HOME - SIEKIN';
                $data['main_content'] = 'home';

                
                $status = "status";
                $tahun = date('Y');

                $data['tipe_a'] = $this->report_model->tipeRSD('a');
                $data['tipe_b'] = $this->report_model->tipeRSD('b');
                $data['tipe_c'] = $this->report_model->tipeRSD('c');
                $data['tipe_d'] = $this->report_model->tipeRSD('d');
                $data['status_belum'] = $this->report_model->statusRSD('belum');
                $data['status_bertahap'] = $this->report_model->statusRSD('bertahap');
                $data['status_penuh'] = $this->report_model->statusRSD('penuh');
                

                
                $this->load->view('template/main',$data);
	}

        
        
}