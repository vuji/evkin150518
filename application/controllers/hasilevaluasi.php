<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Hasilevaluasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('hasilevaluasi_model'));
        $this->load->model(array('master_model'));
    }

    public function index()
    {
        show_404();
    }

    public function rasiokeuangan()
    {
        $data['title'] = 'Hasil Evaluasi - Rasio Keuangan';
        $data['result'] = $this->hasilevaluasi_model->data_keuangan();

        $data['main_content'] = 'hasilevaluasi/rasiokeuangan';
        $this->load->view('template/main', $data);
    }

    public function pelayanan()
    {
        $data['title'] = 'Hasil Evaluasi - Rasio pelayanan';
        $data['result'] = $this->hasilevaluasi_model->data_pelayanan();

        $data['main_content'] = 'hasilevaluasi/aspekpelayanan';
        $this->load->view('template/main', $data);
    }

    public function kepatuhanpengelola()
    {
        $data['title'] = 'Hasil Evaluasi - Kepatuhan Pengelolaan Keuangan RS';
        $data['main_content'] = 'hasilevaluasi/kepatuhanpengelola';

        $this->load->view('template/main', $data);
    }

    public function kualitaslayanan()
    {
        $data['title'] = 'Hasil Evaluasi - Kualitas Layanan';
        $data['main_content'] = 'hasilevaluasi/kualitaslayanan';

        $this->load->view('template/main', $data);
    }

    public function mutumanfaat()
    {
        $data['title'] = 'Hasil Evaluasi - Mutu dan Manfaat';
        $data['main_content'] = 'hasilevaluasi/mutumanfaat';

        $this->load->view('template/main', $data);
    }

    public function penilaian_rasio_keuangan()
    {

        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['result'] = $this->master_model->getDataIndikator('1');
        $data['rsd'] = $this->hasilevaluasi_model->getDataRSD();

        $data['main_content'] = 'penilaian/rasio_keuangan';
        $this->load->view('template/main', $data);

    }

    public function penilaian_rasio_pelayanan()
    {

        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['resultpelayanan'] = $this->master_model->getDataIndikatorAspekPelayanan();
        $data['rsd'] = $this->hasilevaluasi_model->getDataRSD();

        $data['main_content'] = 'penilaian/pelayanan';
        $this->load->view('template/main', $data);
    }

    public function ubah_data_keuangan($id_rsd, $tahun)
    {

        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['result'] = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($id_rsd, $tahun);

        $data['main_content'] = 'penilaian/edit_rasio_keuangan';
        $this->load->view('template/main', $data);

    }

    public function ubah_data_pelayanan($id_rsd, $tahun)
    {

        $data['title'] = 'Siekin - Penilian Rasio pelayanan';
        $data['resultpelayanan'] = $this->hasilevaluasi_model->data_pelayanan_by_rsd_tahun($id_rsd, $tahun);

        $data['main_content'] = 'penilaian/edit_aspek_pelayanan';
        $this->load->view('template/main', $data);

    }

    public function ubah_data_penilaian($id_rsd, $tahun)
    {

        $data['title'] = 'Siekin - Penilian Rasio pelayanan';
        $data['result'] = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($id_rsd, $tahun);
        $data['resultpelayanan'] = $this->hasilevaluasi_model->data_pelayanan_by_rsd_tahun($id_rsd, $tahun);

        $data['main_content'] = 'penilaian/edit_penilaian';
        $this->load->view('template/main', $data);

    }

    public function evaluasi_keuangan()
    {
        $data = array();

        $cek_data = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($this->input->post('id_rsd'), $this->input->post('tahun'));

        if (count($cek_data) > 0) {
            $this->session->set_flashdata('warning', 'rumah sakit sudah di evaluasi');

            redirect('hasilevaluasi/rasiokeuangan');
        } else {

            $jumlah = count($this->input->post('skor'));
            for ($i = 0; $i < $jumlah; $i++) {
                $data = [
                    'id_rsd' => $this->input->post('id_rsd'),
                    'tahun' => $this->input->post('tahun'),
                    'id_indikator' => $this->input->post('id_indikator')[$i],
                    'nilai' => $this->input->post('skor')[$i],
                    'capaian' => $this->input->post('capaian')[$i],
                    'persentase' => $this->input->post('capaian')[$i] / $this->input->post('skor')[$i], //nilai bobot indikator dibagia nilai capaian
                    'penyebab' => $this->input->post('penyebab')[$i],
                ];

                $this->hasilevaluasi_model->simpan_evaluasi($data);
            }

            $this->session->set_flashdata('success', 'aspek keuangan rumah sakit berhasil di evaluasi');

            redirect('hasilevaluasi/rasiokeuangan');
        }

    }

    //evaluasi pelayanan

    public function evaluasi_pelayanan()
    {

        $cek_data = $this->hasilevaluasi_model->data_pelayanan_by_rsd_tahun($this->input->post('id_rsd'), $this->input->post('tahun'));

        if (count($cek_data) > 0) {
            $this->session->set_flashdata('warning', 'rumah sakit sudah di evaluasi');

            redirect('hasilevaluasi/rasiokeuangan');
        } else {

            $jumlah = count($this->input->post('nilai'));
            for ($i = 0; $i < $jumlah; $i++) {
                $data = [
                    'id_rsd' => $this->input->post('id_rsd'),
                    'tahun' => $this->input->post('tahun'),
                    'id_indikator' => $this->input->post('id_indikator')[$i],
                    'nilai' => $this->input->post('nilai')[$i],
                    'capaian' => $this->input->post('capaian')[$i],
                    'persentase' => $this->input->post('nilai')[$i] / $this->input->post('capaian')[$i], //nilai bobot indikator dibagia nilai capaian
                    'penyebab' => $this->input->post('penyebab')[$i],
                ];

                $this->hasilevaluasi_model->simpan_evaluasi($data);
            }

            $this->session->set_flashdata('success', 'aspek keuangan rumah sakit berhasil di evaluasi');

            redirect('hasilevaluasi/pelayanan');
        }

    }

    public function update_evaluasi()
    {

        $data_keuangan = $data_pelayanan = array();

        $jumlah = count($this->input->post('skor_keuangan'));
        for ($i = 0; $i < $jumlah; $i++) {
            $condition = [
                'id_rsd' => $this->input->post('id_rsd'),
                'tahun' => $this->input->post('tahun'),
                'id_indikator' => $this->input->post('id_indikator_keuangan')[$i],
            ];
            $data_keuangan = [
                'nilai' => $this->input->post('skor_keuangan')[$i],
                'capaian' => $this->input->post('capaian_keuangan')[$i],
                'persentase' => $this->input->post('capaian_keuangan')[$i] / $this->input->post('skor_keuangan')[$i],
                'penyebab' => $this->input->post('penyebab_keuangan')[$i],
            ];

            $this->hasilevaluasi_model->update_evaluasi($condition, $data_keuangan);

        }
        $jml = count($this->input->post('nilai_layanan'));
        for ($x = 0; $x < $jml; $x++) {
            $condition = [
                'id_rsd' => $this->input->post('id_rsd'),
                'tahun' => $this->input->post('tahun'),
                'id_indikator' => $this->input->post('id_indikator_layanan')[$x],
            ];
            $data_pelayanan = [
                'nilai' => $this->input->post('nilai_layanan')[$x],
                'capaian' => $this->input->post('capaian_layanan')[$x],
                'persentase' => $this->input->post('capaian_layanan')[$x] / $this->input->post('nilai_layanan')[$x],
                'penyebab' => $this->input->post('penyebab_layanan')[$x],
            ];

            $this->hasilevaluasi_model->update_evaluasi($condition, $data_pelayanan);

        }

        $this->session->set_flashdata('success', 'evalusi aspek keuangan rumah sakit berhasil di ubah');

        redirect('hasilevaluasi/rasiokeuangan');
    }

    public function update_evaluasi_keuangan()
    {
        $data = array();

        $jumlah = count($this->input->post('skor'));
        for ($i = 0; $i < $jumlah; $i++) {
            $condition = [
                'id_rsd' => $this->input->post('id_rsd'),
                'tahun' => $this->input->post('tahun'),
                'id_indikator' => $this->input->post('id_indikator')[$i],
            ];
            $data = [
                'nilai' => $this->input->post('skor')[$i],
                'capaian' => $this->input->post('capaian')[$i],
                'persentase' => $this->input->post('capaian')[$i] / $this->input->post('skor')[$i],
                'penyebab' => $this->input->post('penyebab')[$i],
            ];

            $this->hasilevaluasi_model->update_evaluasi($condition, $data);

        }

        $this->session->set_flashdata('success', 'evalusi aspek keuangan rumah sakit berhasil di ubah');

        redirect('hasilevaluasi/rasiokeuangan');
    }

//update evaluasi aspek pelayanan

    public function update_evaluasi_pelayanan()
    {
        $data = array();

        $jumlah = count($this->input->post('nilai'));
        for ($i = 0; $i < $jumlah; $i++) {
            $condition = [
                'id_rsd' => $this->input->post('id_rsd'),
                'tahun' => $this->input->post('tahun'),
                'id_indikator' => $this->input->post('id_indikator')[$i],
            ];
            $data = [
                'nilai' => $this->input->post('nilai')[$i],
                'capaian' => $this->input->post('capaian')[$i],
                'persentase' => $this->input->post('capaian')[$i] / $this->input->post('nilai')[$i],
                'penyebab' => $this->input->post('penyebab')[$i],
            ];

            $this->hasilevaluasi_model->update_evaluasi($condition, $data);

        }

        $this->session->set_flashdata('success', 'evalusi aspek keuangan rumah sakit berhasil di ubah');

        redirect('hasilevaluasi/pelayanan');
    }

    public function detail_data_keuangan($id_rsd, $tahun)
    {

        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['result'] = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($id_rsd, $tahun);
        $data['resultpelayanan'] = $this->hasilevaluasi_model->data_pelayanan_by_rsd_tahun($id_rsd, $tahun);
        $data['total_bobot'] = 0;
        $data['total_skor'] = 0;

        $data['main_content'] = 'penilaian/detail_rasio_keuangan';

        $this->load->view('template/main', $data);

    }

    public function form_penilaian()
    {
        $data['title'] = 'Siekin - Penilian Rasio Keuangan';
        $data['result_keuangan'] = $this->master_model->getDataIndikator('1');
        $data['result_pelayanan'] = $this->master_model->getDataIndikatorAspekPelayanan();
        $data['rsd'] = $this->hasilevaluasi_model->getDataRSD();

        $data['main_content'] = 'penilaian/form_penilaian';
        $this->load->view('template/main', $data);
    }

    public function proses_evaluasi()
    {
        $data = array();

        $cek_data = $this->hasilevaluasi_model->data_keuangan_by_rsd_tahun($this->input->post('id_rsd'), $this->input->post('tahun'));

        if (count($cek_data) > 0) {
            $this->session->set_flashdata('warning', 'rumah sakit sudah di evaluasi');

            redirect('hasilevaluasi/form_penilaian');
        } else {

            $jumlah = count($this->input->post('skor_keuangan'));
            for ($i = 0; $i < $jumlah; $i++) {
                $data_aspek_keuangan = [
                    'id_rsd' => $this->input->post('id_rsd'),
                    // 'id_perwakilan' => $this->input->post('id_perwakilan'),
                    'tahun' => $this->input->post('tahun'),
                    'id_indikator' => $this->input->post('id_indikator_keuangan')[$i],
                    'nilai' => $this->input->post('skor_keuangan')[$i],
                    'capaian' => $this->input->post('capaian_keuangan')[$i],
                    'persentase' => $this->input->post('capaian_keuangan')[$i] / $this->input->post('skor_keuangan')[$i], //nilai bobot indikator dibagia nilai capaian
                    'penyebab' => $this->input->post('penyebab_keuangan')[$i],
                ];

                $this->hasilevaluasi_model->simpan_evaluasi($data_aspek_keuangan);
            }

            $hitungitu = count($this->input->post('nilai_pelayanan'));
            for ($k = 0; $k < $hitungitu; $k++) {
                $data_aspek_layanan = [
                    'id_rsd' => $this->input->post('id_rsd'),
                    // 'id_perwakilan' => $this->input->post('id_perwakilan'),
                    'tahun' => $this->input->post('tahun'),
                    'id_indikator' => $this->input->post('id_indikator_pelayanan')[$k],
                    'nilai' => $this->input->post('nilai_pelayanan')[$k],
                    'capaian' => $this->input->post('capaian_pelayanan')[$k],
                    'persentase' => $this->input->post('nilai_pelayanan')[$k] / $this->input->post('capaian_pelayanan')[$k], //nilai bobot indikator dibagia nilai capaian
                    'penyebab' => $this->input->post('penyebab_pelayanan')[$k],
                ];

                $this->hasilevaluasi_model->simpan_evaluasi($data_aspek_layanan);
            }

            $this->session->set_flashdata('success', 'aspek keuangan rumah sakit berhasil di evaluasi');

            redirect('hasilevaluasi/rasiokeuangan');
        }
    }
}
