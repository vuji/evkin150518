<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	function __construct(){  
	  parent::__construct();
        $this->load->model('auth_model');
	    
	}

	public function index()
	{
    	$this->session->flashdata('auth'); 
		$this->load->view('user/login_view');
	}

	function login()
	{

		// $this->form_validation->set_rules('username', 'Username');
	 //    $this->form_validation->set_rules('password', 'Password');
	    
	 //    if($this->form_validation->run() == TRUE) 
	 //    {
	      $query = $this->auth_model->validate();
	      $datalogin = $this->auth_model->getDataByUsername($this->input->post('username'), md5($this->input->post('password')));
	      if(count($datalogin)>0)
	      {
	        $data = array(
	          'level' => $datalogin->level,
	          'is_active' => $datalogin->is_active,
	          'username' => $this->input->post('username'),
	          'is_logged_in' => true
	        );
	        $this->session->set_userdata($data);
	        if ($datalogin->level == '1' && $datalogin->is_active == '1')
	        {
	          // $this->session->set_flashdata('notif', "Selamat Datang di Sistem Informasi Kenaikan Gaji Berkala, Anda masuk sebagai Admin");
	          redirect('home');
	        }
	        /* elseif ($datalogin->level == '3') {
	          $this->session->set_flashdata('notif', "Selamat Datang di Sistem Informasi Kenaikan Gaji Berkala, Anda masuk sebagai Pegawai Apoteker");
	          redirect('home');
	        }
	        elseif ($datalogin->level == '2') {
	          $this->session->set_flashdata('notif', "Selamat Datang di Sistem Informasi Kenaikan Gaji Berkala, Anda masuk sebagai Manajer");
	          redirect('home');
	        } */
	      }
	      else // incorrect username or password
	      {
	      	$this->session->set_flashdata('result_login', "Maaf, Username atau Password tidak sesuai. Coba ulangi kembali");
	        redirect('auth');
	      }
	    // }
	    // else
	    // {
	    //   $this->session->set_flashdata('result_login', "Maaf, Username atau Password tidak sesuai. Coba ulangi kembali");
	    //   redirect('auth');
	    // }  
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('auth');
	}
}