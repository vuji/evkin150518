	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Hasil Evaluasi </h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Aspek Keuangan & Layanan</a></li>

							</ol>
						</div>
					</div>
				</div>
			</header>
			<?php $this->load->view('template/flash');?>
			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<a href="<?php echo site_url('hasilevaluasi/form_penilaian') ?>" class="btn btn-inline btn-primary-outline"><i class="fa fa-plus-circle"></i> Tambah</a>
						</div>
					</div>
					<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th class="text-center">No</th>
							<th class="text-center">Nama RSD</th>
							<th class="text-center">Tahun Buku</th>
							<th class="text-center">Bobot</th>
							<th class="text-center">Skor</th>
							<th class="text-center">% Capaian</th>
							<th class="text-center">Aksi</th>
						</tr>
						</thead>
						<tbody>
<?php
$no = 1;
foreach ($result as $row) {
    ?>
								<tr>
									<td align="center"><?php echo $no ?></td>
									<td ><?=$row->nama_rsd?></td>
									<td align="center"><?=$row->tahun?></td>
									<td align="center"><?php

    $nilaix = $row->nilai_keuangan + $row->nilai_layanan;

    echo number_format($nilaix, 2)

    ?></td>
									<td align="center"><?php

    $capaianx = $row->capaian_keuangan + $row->capaian_layanan;

    echo number_format($capaianx, 2)?></td>
									<td align="center"><?php echo number_format(($capaianx / $nilaix) * 100, 2) ?></td>

                      				<td align="center">
                      				<a rel="tooltip" title="Detail Data" class="label label-warning" href="<?=site_url('hasilevaluasi/detail_data_keuangan/' . $row->id_rsd . '/' . $row->tahun)?>"><i class="fa fa-list-alt"></i></a>
                      				<a rel="tooltip" title="Ubah Data" class="label label-warning" href="<?=site_url('hasilevaluasi/ubah_data_penilaian/' . $row->id_rsd . '/' . $row->tahun)?>"><i class="fa fa-edit"></i></a>
                      				</td>



								</tr>


							<?php
$no++;
}?>
						</tbody>
					</table>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script>
		$(function() {
			$('#tabel').DataTable({
				responsive: true
			});
		});


		window.setTimeout(function() {
			$(".alert").fadeTo(300, 0).slideUp(300, function(){
				$(this).remove();
			});
		}, 4000);
</script>

