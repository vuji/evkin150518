	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Hasil Evaluasi Aspek Pelayanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Aspek pelayanan</a></li>
							<!-- 	<li class="active">Rasio pelayanan</li> -->
							</ol>
						</div>
					</div>
				</div>
			</header>
			<?php $this->load->view('template/flash'); ?>
			<section class="card">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<a href="<?php echo site_url('hasilevaluasi/penilaian_rasio_pelayanan') ?>" class="btn btn-inline btn-primary-outline"><i class="fa fa-plus-circle"></i> Tambah</a>
						</div>
					</div>
					<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th data-field="Name" data-sortable="true">No</th>
							<th data-field="Name" data-sortable="true">Nama RSD</th>
							<th data-field="Name" data-sortable="true">Tahun Buku</th>
							<th data-field="Name" data-sortable="true">Bobot</th>
							<th data-field="Name" data-sortable="true">Skor</th>
							<th data-field="Name" data-sortable="true">% Capaian</th>
							<th data-field="Action" data-sortable="true">Aksi</th>
						</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach ($result as $row) { ?>
								<tr>
									<td ><?php echo $no; ?></td>
									<td ><?= $row->nama_rsd ?></td>
									<td ><?= $row->tahun ?></td>
									<td ><?php echo number_format("70",2); ?></td>
									<td ><?php echo number_format($row->capaian,2) ?></td>
									<td ><?php echo number_format(($row->capaian/70)*100,2) ?></td>
									<td ><a href="<?= site_url('hasilevaluasi/ubah_data_pelayanan/'.$row->id_rsd.'/'.$row->tahun) ?>">Ubah Data</a></td>
								</tr>

							<?php 
							$no++;
							} ?>
						</tbody>
					</table>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script>
		$(function() {
			$('#tabel').DataTable({
				responsive: true
			});
		});

		window.setTimeout(function() {
			$(".alert").fadeTo(300, 0).slideUp(300, function(){
				$(this).remove(); 
			});
		}, 4000);
	</script>
 
    