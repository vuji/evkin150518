<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>User</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo base_url(); ?>user">User</a></li>
								<li class="active">Ubah Data User</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-md-8">
							<?php echo form_open('user/update_user/' . $result->id_user, 'role="form"'); ?>
							<input type="hidden" class="form-control" name="id_user" value="<?php echo $result->id_user; ?>" placeholder="ID User">
								<div class="form-group row">
									<label class="col-sm-4 form-control-label" for="signup_v1-username">Username <span class="color-red">*</span></label>
									<div class="col-sm-8">
										<input id="signup_v1-username"
											   class="form-control"
											   name="username"
											   value="<?php echo $result->username; ?>"
											   type="text" data-validation="[L>=4, L<=18, MIXED]"
											   data-validation-message="$ must be between 4 and 18 characters. No special characters allowed."
											   data-validation-regex="/^((?!admin).)*$/i"
											   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
											   <?php echo form_error('username', '<span>', '</span>'); ?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 form-control-label" for="signup_v1-password">Password </label>
									<div class="col-sm-8">
										<input id="signup_v1-password"
											   class="form-control"
											   name="password"
											   type="password" data-validation="[L>=6]"
											   data-validation-message="$ must be at least 6 characters">
											   <?php echo form_error('password', '<span>', '</span>'); ?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 form-control-label" for="signup_v1-password-confirm">Confirm Password </label>
									<div class="col-sm-8">
										<input id="signup_v1-password-confirm"
											   class="form-control"
											   name="password2"
											   value=""
											   type="password" data-validation="[V==signup_v1[password]]"
											   data-validation-message="$ does not match the password">
											   <?php echo form_error('password2', '<span>', '</span>'); ?>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-4 form-control-label" for="signup_v1-nickname-confirm">Nickname <span class="color-red">*</span></label>
									<div class="col-sm-8">
										<input id="signup_v1-nickname"
											   class="form-control"
											   name="nickname"
											   value="<?php echo $result->nickname; ?>"
											   type="text" data-validation="[L>=4, L<=18, MIXED]"
											   data-validation-message="$ must be between 4 and 18 characters. No special characters allowed."
											   data-validation-regex="/^((?!admin).)*$/i"
											   data-validation-regex-message="The word &quot;Admin&quot; is not allowed in the $">
											   <?php echo form_error('nickname', '<span>', '</span>'); ?>
									</div>
								</div>
								<div class="form-group form-group-radios row">
									<label class="col-sm-4 form-control-label" id="signup_v1-gender">
										Hak Akses <span class="color-red">*</span>
									</label>
									<div class="col-sm-4">
										<?php if ($result->level == "1") {
    echo form_radio("level", "1", "checked");
} else {
    echo form_radio("level", "1");
}

?> Admin
					                  <?php if ($result->level == "2") {
    echo form_radio("level", "2", "checked");
} else {
    echo form_radio("level", "2");
}

?> Pegawai
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-8">
										<a class="btn btn-inline btn-secondary" onClick="history.go(-1);return true;">Kembali</a>
										<button type="submit" class="btn btn-inline">Simpan</button>
										<a class="btn btn-inline btn-success" onClick="window.location.reload();return true;">Refresh</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script type="text/javascript">
		$(function() {
			$('#form-signup_v1').validate({
				submit: {
					settings: {
						inputContainer: '.form-group',
						errorListClass: 'form-tooltip-error'
					}
				}
			});
		});
	</script>