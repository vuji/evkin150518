<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SIEKIN</title>

    <link href="<?php echo base_url(); ?>assets/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="<?php echo base_url(); ?>assets/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="<?php echo base_url(); ?>assets/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="<?php echo base_url(); ?>assets/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="<?php echo base_url(); ?>assets/img/favicon.png" rel="icon" type="image/png">
    <link href="<?php echo base_url(); ?>assets/img/favicon.ico" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <style type="text/css">
        body{
                background: #659AC0;
        }
    </style>
</head>
<body>

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" action="<?php echo base_url();?>auth/login" method="post">
                    <div class="sign-avatar">
                        <img src="<?php echo base_url(); ?>assets/img/logo-bpkp.png" alt="">
                    </div>
                    <header class="sign-title">Aplikasi Hasil Evaluasi Kinerja RSD-BLUD</header>
                    <?php if($this->session->flashdata('result_login')) { ?>
                        <div class="alert alert-danger alert-no-border alert-close alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <i class="font-icon font-icon-inline font-icon-warning"></i>
                            <strong></strong><br/>
                              <?php echo $this->session->flashdata('result_login');?>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" value="<?php echo set_value('username');?>" id="username" placeholder="Username"/>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                    </div>
                    <button type="submit" class="btn btn-rounded">Sign In</button>
                </form>
            </div>
        </div>
    </div>

<script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="<?php echo base_url(); ?>assets/js/app.js"></script>
</body>
</html>