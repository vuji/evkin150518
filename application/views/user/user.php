	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Data User</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">User</a></li>
								<li class="active">Data User</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<a type="button" class="btn btn-inline btn-primary-outline" href="<?php echo base_url(); ?>user/add_user"><i class="fa fa-plus-circle"></i>Tambah</a>
						</div>
					</div>
					<table id="tabeluser" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th data-field="Name" data-sortable="true">No</th>
							<th data-field="Name" data-sortable="true">Username</th>
							<th data-field="Name" data-sortable="true">Nickname</th>
							<th data-field="Name" data-sortable="true">Level User</th>
							<th data-field="Action" data-sortable="true">Aksi</th>
						</tr>
						</thead>
						<tbody>
							<?php
$no = 1;
foreach ($result as $data) {
    ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?=$data->username;?></td>
								<td><?=$data->nickname;?></td>
								<td><?php if ($data->level == 1) {echo "Admin";} else {
        echo "User";
    }
    ?></td>
								<td>
									<a type="button" class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data" href="<?php echo base_url(); ?>user/update_user/<?php echo $data->id_user; ?>"><i class="fa fa-edit"></i></a>
									<a type="button" class="btn btn-danger-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Data" href="<?php echo base_url(); ?>user/delete_user/<?php echo $data->id_user; ?>"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<?php
$no++;
}?>
						</tbody>
					</table>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script>
		$(function() {
			$('#tabeluser tfoot th').each(function() {
	        	var title = $(this).text();
	        	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	    	} );

		    // DataTable
		    var table = $('#tabeluser').DataTable();

		    // Apply the search
		    table.columns().every(function() {
		        var that = this;

		        $('input', this.footer() ).on( 'keyup change', function () {
		            if (that.search() !== this.value) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		});
	</script>