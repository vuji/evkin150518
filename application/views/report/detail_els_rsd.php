 <?php

header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

 <table border="1" width="100%">
    <thead>
        <tr>
            <th colspan="6">Aspek Keuangan</th>
        </tr>
        <tr>
            <th>Nama Subaspek</th>
            <th>Nama Indikator</th>
            <th>Bobot</th>
            <th>Skor</th>
            <th>%Capaian</th>
            <th>Penyebab</th>
        </tr>
    </thead>
    <tbody>
        <?php $nasubpek = '';?>
        <?php if ($result) {
    ?>
            <?php foreach ($result as $data) {
        ?>
                <tr>
                    <td><?=$data->nama_subaspek != $nasubpek ? $data->nama_subaspek : '';?></td>
                    <td><?=$data->nama_indikator;?></td>
                    <td>
                        <?=$data->bobot;?>
                        <input type="hidden" name="skor[]" value="<?=$data->bobot;?>">
                        <input type="hidden" name="id_indikator[]" value="<?=$data->id_indikator;?>">
                    </td>
                    <td>
                        <?=$data->capaian;?>
                    </td>
                    <td>
                        <span><?=round($data->persentase, 2) * 100?></span>

                    </td>
                    <td>
                        <?=$data->penyebab;?>
                    </td>
                </tr>
                <?php

        $nasubpek = $data->nama_subaspek;

        $total_bobot += $data->bobot;
        $total_skor += $data->capaian;

    }?>
        <?php } else {
    ?>

            <tr>
                <td  colspan="7" class="text-center">data tidak tersedia</td>
            </tr>

        <?php }?>


        <tr>
            <td colspan="2">Total</td>
            <td>
                <?php if ($total_bobot == 0) {
    echo "0";
} else {
    echo ($total_bobot);
}?>
            </td>

            <td colspan="3">
                <?php if ($total_skor == 0) {
    echo "0";
} else {
    echo ($total_skor);
}?>


            </td>
        </tr>
        <tr>
            <th colspan="7">Aspek Pelayanan</th>
        </tr>

        <tr>
            <th>Subaspek</th>
            <th>Kelompok Indikator</th>
            <th>Indikator</th>
            <th>Bobot</th>
            <th>Skor</th>
            <th>% Capaian</th>
            <th>Penyebab</th>
        </tr>


        <?php $namasubpek = $namaind = '';?>
        <?php if ($resultpelayanan) {
    ?>
            <?php foreach ($resultpelayanan as $data) {
        ?>
                <tr>
                    <td><?=$data->nama_subaspek != $namasubpek ? $data->nama_subaspek : '';?></td>
                    <td><?=$data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
                    <td><?=$data->indikator_child_name;?></td>
                    <td>
                        <?=$data->bobot;?>
                        <input type="hidden"  name="nilai[]" value="<?=$data->bobot?>">
                        <input type="hidden"  name="id_indikator[]" value="<?=$data->id_child_indikator?>">
                    </td>
                    <td>

                        <span><?=$data->capaian?></span>
                    </td>
                    <td>
                        <span><?=round($data->persentase, 2) * 100?></span>

                    </td>
                    <td>
                        <span><?=$data->penyebab?></span>
                    </td>
                </tr>
                <?php

        $namasubpek = $data->nama_subaspek;
        $namaind = $data->indikator_parent_name;

    }?>
        <?php } else {?>

            <tr>
                <td  colspan="3" class="text-center">data tidak tersedia</td>
            </tr>

        <?php }?>

    </tbody>
</table>