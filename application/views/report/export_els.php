 <?php

header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>



<table border="1" width="100%">
                        <thead>
                            <tr>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">No</th>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">Nama RSD</th>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">Tahun</th>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">Total Skor Aspek Keuangan</th>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">Total Skor Aspek Pelayanan</th>
                                <th style="font-size: 11px;" data-field="Name" data-sortable="true">Jumlah Total Skor</th>
                                <th style="font-size: 11px;" data-field="Action" data-sortable="true">Kategori</th>
                                <th style="font-size: 11px;" data-field="Action" data-sortable="true">Sub Kategori</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
$sum = 0;
foreach ($result as $row) {
    ?>
                                <tr>
                                    <td style="font-size: 11px;" align="center"><?php echo $no; ?></td>
                                    <td style="font-size: 11px;" ><?=$row->nama_rsd?></td>
                                    <td style="font-size: 11px;" ><?=$row->tahun?></td>
                                    <td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_keuangan, 2) ?></td>
                                    <td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_layanan, 2) ?></td>
                                    <td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_keuangan + $row->capaian_layanan, 2) ?></td>
                                    <!-- <td style="font-size: 11px;" ><?=$row->tipe?></td> -->
                                    <td style="font-size: 11px;" align="center">
                                        <?php $sum = number_format($row->capaian_keuangan + $row->capaian_layanan, 2);
    if ($sum > 65) {echo "BAIK";}
    if ($sum > 30 && $sum <= 65) {echo "SEDANG";}
    if ($sum <= 30) {echo "BURUK";}

    ?>
</td>
                                        <td style="font-size: 11px;" align="center">
                                            <?php
$sum = number_format($row->capaian_layanan + $row->capaian_keuangan, 2);
    if ($sum > 95) {echo "AAA";}
    if ($sum > 80 && $sum <= 95) {echo "AA";}
    if ($sum > 65 && $sum <= 80) {echo "A";}
    if ($sum > 50 && $sum <= 65) {echo "BBB";}
    if ($sum > 40 && $sum <= 50) {echo "BB";}
    if ($sum > 30 && $sum <= 40) {echo "B";}
    if ($sum > 15 && $sum <= 30) {echo "CC";}
    if ($sum < 15) {echo "C";}

    ?>
                                        </td>

                                    </tr>

                                    <?php
$no++;
}?>
                            </tbody>
                        </table>