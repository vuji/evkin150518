<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
            <tr>
                <th class="header">No. </th>
                <th class="header">Nama RSD</th>                           
                <th class="header">Tahun</th>                      
                <th class="header">Total Skor Aspek Keuangan</th>
                <th class="header">Total Skor Aspek Pelayanan</th>
                <th class="header">Jumlah Total Skor</th>
                <th class="header">Kategori</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($exportExcel) && !empty($exportExcel)) {
                foreach ($exportExcel as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['no']; ?></td>   
                        <td><?php echo $element['nama_rsd']; ?></td> 
                        <td><?php echo $element['tahun']; ?></td>                       
                        <td><?php echo $element['dob']; ?></td>
                        <td><?php echo $element['contact_no']; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">There is no employee.</td>    
                </tr>
            <?php } ?>

        </tbody>
    </table>
    <a class="pull-right btn btn-primary btn-xs" href="<?php echo site_url()?>export/createxls"><i class="fa fa-file-excel-o"></i> Export Data</a>
</div> 