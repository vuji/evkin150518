	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Evaluasi Kinerja RSD-BLUD</h3>
							<ol class="breadcrumb breadcrumb-simple">

							</ol>
						</div>
					</div>
				</div>
			</header>
			<?php $this->load->view('template/flash');?>


			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<h5 class="with-border">Data XYZ</h5>

					<form method="GET">

						<div class="row">
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-3 form-label semibold">Provinsi</label>
									<div class="col-sm-9">
										<select name="id_provinsi" id="id_provinsi" class="form-control">
											<option value="">--Pilih--</option>
											<?php foreach ($provinsi as $k) {?>
												<option value="<?php echo $k->id_provinsi; ?>" <?php echo ($provinsi == $k->id_provinsi) ? 'selected' : '' ?> ><?php echo $k->nama_provinsi; ?></option>
											<?php }?>
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-3 form-label semibold">Perwakilan</label>
									<div class="col-sm-9">
										<select name="id_perwakilan" id="id_perwakilan" class="form-control">
											<option value="">--Pilih--</option>
											<?php foreach ($perwakilan as $k) {?>
												<option value="<?php echo $k->id_perwakilan; ?>" <?php echo ($perwakilan == $k->id_perwakilan) ? 'selected' : '' ?> ><?php echo $k->nama_perwakilan; ?></option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row">
									<label class="col-sm-3 form-label semibold">RSD</label>
									<div class="col-sm-9">
										<select name="id_rsd" id="id_rsd" class="form-control">
											<option value="">--Pilih--</option>
											<?php if (count($rsd) != 0) {foreach ($rsd as $k) {?>
												<option value="<?php echo $k->id_rsd; ?>" data-perwakilan="<?php echo $k->id_perwakilan ?>" ><?php echo $k->nama_rsd; ?></option>
											<?php }}?>
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-3 form-label semibold">Tahun</label>
									<div class="col-sm-9">
										<select class="form-control" name="tahun">
											<option value="" selected >--Pilih--</option>
											<?php $year = date("Y");for ($x = 2016; $x <= $year; $x++) {echo "<option value=$x>$x</option>";}?>
										</select>
									</div>
								</div>


							</div>
							<div class="col-md-12">

								<button type="reset" class="btn btn-warning"><i class="fa fa-refresh"></i> batal</button>

								<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>

								<?php

if (isset($_GET)) {
    $getnya = array();
    foreach ($_GET as $key => $value) {

        $getnya[] = $key . "=" . $value;

    }

    $params = (count($_GET) >= 1) ? implode("&", $getnya) : '';

}

?>

								<a href="<?php echo base_url() ?>export?<?php echo $params ?>" class="btn btn-primary float-right"><i class="fa fa-file-excel-o"></i> Export</a>
							</div>

						</div>
					</form>
				</div>

				<div class="card-block table-responsive">
					<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">No</th>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">Nama RSD</th>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">Tahun</th>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">Total Skor Aspek Keuangan</th>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">Total Skor Aspek Pelayanan</th>
								<th style="font-size: 11px;" data-field="Name" data-sortable="true">Jumlah Total Skor</th>
								<th style="font-size: 11px;" data-field="Action" data-sortable="true">Kategori</th>
								<th style="font-size: 11px;" data-field="Action" data-sortable="true">Sub Kategori</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
$sum = 0;
foreach ($result as $row) {
    ?>
								<tr>
									<td style="font-size: 11px;" align="center"><?php echo $no; ?></td>
									<td style="font-size: 11px;" ><?=$row->nama_rsd?></td>
									<td style="font-size: 11px;" ><?=$row->tahun?></td>
									<td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_keuangan, 2) ?></td>
									<td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_layanan, 2) ?></td>
									<td style="font-size: 11px;" align="center"><?php echo number_format($row->capaian_keuangan + $row->capaian_layanan, 2) ?></td>
									<!-- <td style="font-size: 11px;" ><?=$row->tipe?></td> -->
									<td style="font-size: 11px;" align="center">
										<?php $sum = number_format($row->capaian_keuangan + $row->capaian_layanan, 2);
    if ($sum > 65) {echo "BAIK";}
    if ($sum > 30 && $sum <= 65) {echo "SEDANG";}
    if ($sum <= 30) {echo "BURUK";}

    ?>
									</td>
									<td style="font-size: 11px;" align="center">
										<?php
$sum = number_format($row->capaian_layanan + $row->capaian_keuangan, 2);
    if ($sum > 95) {echo "AAA";}
    if ($sum > 80 && $sum <= 95) {echo "AA";}
    if ($sum > 65 && $sum <= 80) {echo "A";}
    if ($sum > 50 && $sum <= 65) {echo "BBB";}
    if ($sum > 40 && $sum <= 50) {echo "BB";}
    if ($sum > 30 && $sum <= 40) {echo "B";}
    if ($sum > 15 && $sum <= 30) {echo "CC";}
    if ($sum < 15) {echo "C";}

    ?>
									</td>

								</tr>

								<?php
$no++;
}?>
						</tbody>
					</table>
				</div>
			</section>

			<!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script>
		$(function() {
			$('#tabel').DataTable({
				responsive: true,
				searching: false,
			});
		});


		window.setTimeout(function() {
			$(".alert").fadeTo(300, 0).slideUp(300, function(){
				$(this).remove();
			});
		}, 4000);
	</script>


	<script type="text/javascript">
	$('#id_perwakilan').change(function(e) {
		var id_perwakilan = e.target.value;

// 		$.get("<?php echo base_url() ?>master/rsd_lists/" + id_perwakilan, function(data) {
// 			console.log(data);
// 			$('#id_rsd').empty();
// 			$.each(data,function(key, value)
// {
//     $('#id_rsd').append('<option value=' + key + '>' + value + '</option>');
// });
// 			// $.each(data, function(key, value) {
// 			// 	var option = $("<option></option>")
// 	  //                 .attr("value", key)
// 	  //                 .text(value);

// 			// 	$('#id_rsd').append(option);
// 			// });
// 		});
//
 $.ajax({
       url:'<?php echo base_url() ?>master/rsd_lists/' + id_perwakilan,
       method: 'get',
       dataType: 'json',
       success: function(data){

         // Remove options
         $('#id_rsd').find('option').not(':first').remove();

         // Add options
         $.each(data,function(key,namax){
           $('#id_rsd').append('<option value="'+key+'">'+namax+'</option>');
         });
       }
    });
  });

</script>

