<div class="mobile-menu-left-overlay"></div>
	<nav class="side-menu">
		<div class="side-menu-avatar">
	        <div class="avatar-preview avatar-preview-64">
	            <img src="<?php echo base_url('assets/img/avatar-1-128.png');?>" alt="">
	            <label href=""><?php echo $this->session->userdata('username'); ?></label>
        	</div>
	    </div>
	    <ul class="side-menu-list">
	        <li class="brown<?php if($this->uri->segment(1)=="home") {echo " opened";} ?>">
	            <a href="<?php echo base_url();?>home">
	                <i class="font-icon font-icon-home"></i>
	                <span class="lbl">Dashboard</span>
	            </a>
	        </li>
	        
	        <li class="purple with-sub<?php if(in_array($this->uri->segment(2), ['perwakilan','provinsi','rsd','aspek','subaspek'])) {echo " opened";}  else { echo " ";}  ?>">
	            <span>
	                <i class="font-icon font-icon-page"></i>
	                <span class="lbl">Preferensi</span>
	            </span>
	            <ul>
	                <li><a href="<?php echo base_url();?>master/perwakilan"><span class="lbl">Perwakilan</span></a></li>
	                <li><a href="<?php echo base_url();?>master/provinsi"><span class="lbl">Provinsi</span></a></li>
	                <li><a href="<?php echo base_url();?>master/rsd"><span class="lbl">Rumah Sakit</span></a></li>
	                <li><a href="<?php echo base_url();?>master/aspek"><span class="lbl">Aspek</span></a></li>
	                <li><a href="<?php echo base_url();?>master/subaspek"><span class="lbl">Sub Aspek</span></a></li>
	                <!-- <li><a href="<?php echo base_url();?>master/kelindikator"><span class="lbl">Kelompok Indikator</span></a></li> -->
	                <li><a href="<?php echo base_url();?>master/indikator"><span class="lbl">Indikator</span></a></li>
	            </ul>
	        </li>

	        <li class="blue-dirty with-sub<?php if(in_array($this->uri->segment(2), ['rasiokeuangan','pelayanan'])) {echo " opened";} else { echo " ";} ?>">
	            	<a href="<?php echo base_url();?>hasilevaluasi/rasiokeuangan">
	                <i class="font-icon font-icon-page"></i>
	                <span class="lbl">Hasil Evaluasi</span>
	            	</a>
	           <!--  <ul>
	                <li><a href="<?php echo base_url();?>hasilevaluasi/rasiokeuangan"><span class="lbl">Aspek Keuangan</span></a></li>
	                <li><a href="<?php echo base_url();?>hasilevaluasi/pelayanan"><span class="lbl">Aspek Pelayanan</span></a></li>
	            </ul> -->
	        </li>
	        
	        <li class="pink-red<?php if($this->uri->segment(1)=="report") {echo " opened";} else { echo " ";}  ?>">
	            <a href="<?php echo base_url();?>report/laporan_kompilasi">
	                <i class="font-icon font-icon-chart"></i>
	                <span class="lbl">Laporan</span>
	            </a>
	        </li>
	        
	        <li class="aquamarine<?php if($this->uri->segment(1)=="user") {echo " opened";} else { echo " ";}  ?>">
	            <a href="<?php echo base_url();?>user">
	                <i class="font-icon font-icon-users"></i>
	                <span class="lbl">Kelola User</span>
	            </a>
	        </li>
	    </ul>
	</nav><!--.side-menu-->