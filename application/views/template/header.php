<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?=$title?></title>


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="<?=base_url('assets/css/lib/font-awesome/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/lib/bootstrap/bootstrap.min.css')?>">

	<link rel="stylesheet" href="<?=base_url('assets/css/separate/vendor/datatables-net.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/main.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/custom.css')?>">
    <script src="<?=base_url('assets/js/lib/jquery/jquery.min.js')?> "></script>
    <script src="<?=base_url('assets/js/popper.min.js')?> "></script>


    <!--datatables -->
     <link rel="stylesheet" href="<?=base_url('assets/vendor/DataTables/datatables.min.css')?>">
     <link rel="stylesheet" href="<?=base_url('assets/vendor/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')?>">
     <link rel="stylesheet" href="<?=base_url('assets/vendor/DataTables/Buttons-1.5.2/css/buttons.bootstrap4.min.css')?>">

    <script src="<?=base_url('assets/vendor/DataTables/datatables.min.js')?> "></script>
    <script src="<?=base_url('assets/vendor/DataTables/Buttons-1.5.2/js/buttons.bootstrap4.min.js')?> "></script>
    <script src="<?=base_url('assets/vendor/DataTables/datatables.min.js')?> "></script>


	<script>
		$(function() {
			$('#example').DataTable();
		});
	</script>

	    <!-- / datatables -->

