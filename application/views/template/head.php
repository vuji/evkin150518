</head>
<body class="with-side-menu">

	<header class="site-header">
	    <div class="container-fluid">

	        <a href="#" class="site-logo">
	          	            <img class="hidden-lg-up" src="<?php echo base_url('assets/img/logo-bpkp.png'); ?>" alt="">
	        </a>

	        <a id="show-hide-sidebar-toggle" class="show-hide-sidebar">
	            <span>toggle menu</span>
	        </a>

	        <a class="hamburger hamburger--htla">
	            <span>toggle menu</span>
	        </a>
	        <div class="site-header-content">
	            <div class="site-header-content-in">
	                <div class="site-header-shown">
	                    <div class="dropdown user-menu">
	                        <a class="dropdown-toggle" id="dd-user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <img src="<?php echo base_url('assets/img/avatar-2-64.png'); ?>" alt="">
	                        </a>

	                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
	                           <!--  <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
	                            <div class="dropdown-divider"></div> -->
	                            <a class="dropdown-item" href="<?php echo base_url(); ?>auth/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
	                        </div>
	                    </div>
	                </div><!--.site-header-shown-->

	                <div class="mobile-menu-right-overlay"></div>
	                <div class="site-header-collapsed">
	                    <div class="site-header-collapsed-in">
	                    </div><!--.site-header-collapsed-in-->
	                </div><!--.site-header-collapsed-->
	            </div><!--site-header-content-in-->
	        </div><!--.site-header-content-->
	    </div><!--.container-fluid-->
	</header><!--.site-header-->