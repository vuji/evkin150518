	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Master</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Aspek</a></li>
								<li class="active">Aspek</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<button class="btn btn-inline btn-primary-outline" onclick="add()"><i class="fa fa-plus-circle"></i>Tambah</button>
						</div>
					</div>
					<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th data-field="No" data-sortable="true">No</th>
							<th data-field="Name" data-sortable="true">Nama Aspek</th>
							<th data-field="Action" data-sortable="true">Aksi</th>
						</tr>
						</thead>
						<tbody>
							<?php
$no = 1;
foreach ($result as $data) {
    ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?=$data->nama_aspek;?></td>
								<td>

									<button class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data" onclick="update(<?php echo $data->id_aspek; ?>)"><i class="fa fa-edit"></i></button>
									<button class="btn btn-danger-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="del(<?php echo $data->id_aspek; ?>)"><i class="fa fa-trash-o"></i></button>
								</td>
							</tr>
							<?php
$no++;
}?>
						</tbody>
					</table>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabel').DataTable({
				responsive: true
			});
		});
		var save_method;
		function add()
		{
			save_method = 'add';
			$('#form')[0].reset();
      		$('#modal_form').modal('show');
		}

		function update(id)
		{
			save_method = 'update';
			$('#form')[0].reset();
			$.ajax({
			url : "<?php echo site_url('master/getIdAspek') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
			    $('[name="id_aspek"]').val(data.id_aspek);
			    $('[name="nama_aspek"]').val(data.nama_aspek);

			    $('#modal_form').modal('show');

			    $('.modal-title').text('Update Data');

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
			    alert('Error get data from ajax');
			}
			});
		}

	    function save()
	    {
	      	var url;
	      	if(save_method == 'add')
      		{
      			url = "<?php echo site_url('master/add_aspek') ?>";
	      	} else {
	        	url = "<?php echo site_url('master/update_aspek') ?>";
	    	}
	          $.ajax({
	            url : url,
	            type: "POST",
	            data: $('#form').serialize(),
	            dataType: "JSON",
	            success: function(data)
	            {
	               if(data.status)//if success close modal and reload ajax table
	               	{
	               		$('#modal_form').modal('hide');
	               		location.reload();// for reload a page
	          		}
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Error adding / update data');
	            }
	        });
	    }

        function del(id)
	    {
			if(confirm('Apakah Anda yakin data akan dihapus?'))
			{
			// ajax delete data from database
			  $.ajax({
			    url : "<?php echo site_url('master/del_aspek') ?>/"+id,
			    type: "POST",
			    dataType: "JSON",
			    success: function(data)
			    {
					$('#modal_form').modal('hide');
					location.reload();
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			        alert('Error deleting data');
			    }
			  });
			}
	  	}
	</script>

    <!-- ============ MODAL UPDATE =============== -->

    <div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel"></h3>
        </div>
        <form class="form-horizontal" method="post" action="#" id="form">
            <div class="modal-body">
                <input type="hidden" name="id_aspek" value="" class="form-control" type="text" placeholder="ID Aspek..." readonly>
                <div class="row form-group">
                    <label class="control-label col-sm-3" >Nama Aspek</label>
                    <div class="col-sm-8">
                        <input name="nama_aspek" class="form-control" type="text" placeholder="Nama Aspek..." required>
                    </div>
                </div>

             </div>
        </form>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Tutup</button>
                <button class="btn" onclick="save()">Simpan</button>
            </div>
        </div>
        </div>
    </div>

    <?php //endforeach;?>
    <!--END MODAL-->