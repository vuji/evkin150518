	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Master</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Indikator</a></li>
								<li class="active">Indikator</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<button class="btn btn-inline btn-primary-outline" onclick="add()"><i class="fa fa-plus-circle"></i>Tambah</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<fieldset >
								<legend >Aspek Keuangan</legend>
								<div class="table-responsive">
									<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
										<thead>
											<tr>
												<th data-field="Name" data-sortable="true">Subaspek</th>
												<th data-field="Name" data-sortable="true">Indikator</th>
												<th data-field="Bobot" data-sortable="true">Bobot</th>
												<th data-field="Action" data-sortable="true">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $nasubpek = '';?>
											<?php if ($resultkeuangan) {
    ?>
												<?php foreach ($resultkeuangan as $data) {
        ?>
													<tr>
														<td><?=$data->nama_subaspek != $nasubpek ? $data->nama_subaspek : '';?></td>
														<td><?=$data->nama_indikator;?></td>
														<td><?=$data->bobot;?></td>
														<td nowrap>

															<button class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data" onclick="update(<?php echo $data->id_indikator; ?>)"><i class="fa fa-edit"></i></button>
															<button class="btn btn-danger-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="del(<?php echo $data->id_indikator; ?>)"><i class="fa fa-trash-o"></i></button>
														</td>
													</tr>
													<?php

        $nasubpek = $data->nama_subaspek;

    }?>
											<?php } else {?>

												<tr>
													<td  colspan="3" class="text-center">data tidak tersedia</td>
												</tr>

											<?php }?>
										</tbody>
									</table>
								</div>
							</fieldset>
						</div>

						<!-- / aspek keuangan -->

						<div class="col-md-6">
							<fieldset >
								<legend >Aspek Pelayanan</legend>
								<div class="table-responsive">
									<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
										<thead>
											<tr>
												<th data-field="Name" data-sortable="true">Subaspek</th>
												<th data-field="Name" data-sortable="true">Kelompok Indikator</th>
												<th data-field="Name" data-sortable="true">Indikator</th>
												<th data-field="Bobot" data-sortable="true">Bobot</th>
												<th data-field="Action" data-sortable="true">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $namasubpek = $namaind = '';?>
											<?php if ($resultpelayanan) {
    ?>
												<?php foreach ($resultpelayanan as $data) {
        ?>
													<tr>
														<td><?=$data->nama_subaspek != $namasubpek ? $data->nama_subaspek : '';?></td>
														<td><?=$data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
														<td><?=$data->indikator_child_name;?></td>
														<td><?=$data->bobot;?></td>
														<td nowrap>

															<button class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data" onclick="update(<?php echo $data->id_child_indikator; ?>)"><i class="fa fa-edit"></i></button>
															<button class="btn btn-danger-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="del(<?php echo $data->id_child_indikator; ?>)"><i class="fa fa-trash-o"></i></button>
														</td>
													</tr>
													<?php

        $namasubpek = $data->nama_subaspek;
        $namaind = $data->indikator_parent_name;

    }?>
											<?php } else {?>

												<tr>
													<td  colspan="3" class="text-center">data tidak tersedia</td>
												</tr>

											<?php }?>
										</tbody>
									</table>
								</div>
							</fieldset>
						</div>

						<!-- /aspek pelayanan-->

					</div>

				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script type="text/javascript">
		// $(document).ready(function() {
		// 	$('#tabel').DataTable({
		// 		responsive: true
		// 	});
		// });
		var save_method;
		function add()
		{
			save_method = 'add';
			$('#form')[0].reset();
			$('#modal_form').modal('show');
		}

		function update(id)
		{
			save_method = 'update';
			$('#form')[0].reset();
			$.ajax({
				url : "<?php echo site_url('master/getIdIndikator') ?>/" + id,
				type: "GET",
				dataType: "JSON",
				success: function(data)
				{
					$('[name="id_indikator"]').val(data.id_indikator);
					$('[name="id_kelindikator"]').val(data.id_kelindikator);
					$('[name="nama_indikator"]').val(data.nama_indikator);
					$('[name="bobot"]').val(data.bobot);

					$('#modal_form').modal('show');

					$('.modal-title').text('Update Data');

				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error get data from ajax');
				}
			});
		}

		function save()
		{
			var url;
			if(save_method == 'add')
			{
				url = "<?php echo site_url('master/add_indikator') ?>";
			} else {
				url = "<?php echo site_url('master/update_indikator') ?>";
			}
			$.ajax({
				url : url,
				type: "POST",
				data: $('#form').serialize(),
				dataType: "JSON",
				success: function(data)
				{
	               if(data.status)//if success close modal and reload ajax table
	               {
	               	$('#modal_form').modal('hide');
	               		location.reload();// for reload a page
	               	}
	               },
	               error: function (jqXHR, textStatus, errorThrown)
	               {
	               	alert('Error adding');
	               }
	           });
		}

		function del(id)
		{
			if(confirm('Apakah Anda yakin data akan dihapus?'))
			{
			// ajax delete data from database
			$.ajax({
				url : "<?php echo site_url('master/del_indikator') ?>/"+id,
				type: "POST",
				dataType: "JSON",
				success: function(data)
				{
					$('#modal_form').modal('hide');
					location.reload();
				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error deleting data');
				}
			});
		}
	}
</script>

<!-- ============ MODAL UPDATE =============== -->

<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 class="modal-title" id="myModalLabel"></h3>
			</div>
			<form class="form-horizontal" method="post" action="#" id="form">
				<div class="modal-body">
					<input type="hidden" name="id_indikator" value="" class="form-control" type="text" placeholder="ID indikator..." readonly>

					<div class="row form-group">
						<label class="control-label col-sm-3" >Sub Aspek</label>
						<div class="col-sm-8">
							<select name="id_subaspek" class="form-control" required>
								<option value="">--Pilih Sub Aspek--</option>
								<?php foreach ($subaspek as $k) {
    echo '<option value="' . $k->id_subaspek . '">' . $k->nama_subaspek . '</option>';
}?>
							</select>
						</div>
					</div>


                <!-- <div class="row form-group">
                    <label class="control-label col-sm-3" >Apakah punya kelompok indikator?</label>
                    <div class="col-sm-8">
                        <input name="id_kelindikator" class="form-control" type="checkbox" placeholder="Indikator..." required>
                    </div>
                </div> -->

                <div class="row form-group">
                	<label class="control-label col-sm-3" >Indikator</label>
                	<div class="col-sm-8">
                		<input name="nama_indikator" class="form-control" type="text" placeholder="Indikator..." required>
                	</div>
                </div>

                <div class="row form-group">
                	<label class="control-label col-sm-3" >Bobot</label>
                	<div class="col-sm-8">
                		<label>Kosongi bobot jika termasuk Kelompok Indikator</label>
                		<input name="bobot" class="form-control" type="text" placeholder="Bobots...">
                	</div>
                </div>

                <div class="row form-group">
                	<label class="control-label col-sm-3" >Kelompok Indikator</label>
                	<div class="col-sm-8">
                		<select name="id_kelindikator" class="form-control" required>
                			<option value="">-- not set--</option>
                			<?php foreach ($kelindikator as $k) {
    echo '<option value="' . $k->id_indikator . '">' . $k->nama_indikator . '</option>';
}?>
                		</select>
                	</div>
                </div>


            </div>
        </form>

        <div class="modal-footer">
        	<button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Tutup</button>
        	<button class="btn" onclick="save()">Simpan</button>
        </div>
    </div>
</div>
</div>

<?php //endforeach;?>
    <!--END MODAL-->