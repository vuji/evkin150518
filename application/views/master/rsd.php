	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Master</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Rumah Sakit</a></li>
								<li class="active">Rumah Sakit</li>
							</ol>
						</div>
					</div>
				</div>
			</header>
			<?php $this->load->view('template/flash');?>
			<section class="card" style="padding: 30px;">
				<div class="card-block">
					<div class="row">
						<div class="col-sm-2">
							<button class="btn btn-inline btn-primary-outline" onclick="add()"><i class="fa fa-plus-circle"></i>Tambah</button>
						</div>
					</div>
					<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th data-field="Name" data-sortable="true">No</th>
							<th data-field="Name" data-sortable="true">Nama Rumah Sakit</th>
							<th data-field="Name" data-sortable="true">Pemilik RSD</th>
							<th data-field="Name" data-sortable="true">Tipe Rumah Sakit</th>
							<th data-field="Name" data-sortable="true">Status BLUD RSD</th>
							<th data-field="Action" data-sortable="true">Aksi</th>
						</tr>
						</thead>
						<tbody>
							<?php foreach ($result as $data) {?>
							<tr>
								<td><?=$data->id_rsd;?></td>
								<td><?=$data->nama_rsd;?></td>
								<td><?=$data->pemilik;?></td>
								<td><?=$data->tipe;?></td>
								<td><?=$data->status;?></td>
								<td>
									<!-- <span data-toggle="modal" data-target="#<?php echo $data->id_grupsk; ?>">
										<a type="button" class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data"><i class="fa fa-edit"></i></a>
									</span> -->
									<button class="btn btn-secondary-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data" onclick="update(<?php echo $data->id_rsd; ?>)"><i class="fa fa-edit"></i></button>
									<button class="btn btn-danger-outline btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Data" onclick="del(<?php echo $data->id_rsd; ?>)"><i class="fa fa-trash-o"></i></button>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabel').DataTable({
				responsive: true
			});
		});
		var save_method;
		function add()
		{
			save_method = 'add';
			$('#form')[0].reset();
      		$('#modal_form').modal('show');
		}

		function update(id)
		{
			save_method = 'update';
			$('#form')[0].reset();
			$.ajax({
			url : "<?php echo site_url('master/getIdRSD') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
			    $('[name="id_rsd"]').val(data.id_rsd);
			    $('[name="nama_rsd"]').val(data.nama_rsd);
			    $('[name="pemilik"]').val(data.pemilik);
			    $('[name="tipe"]').val(data.tipe);
			    $('[name="status"]').val(data.status);
			    $('[name="id_perwakilan"]').val(data.id_perwakilan);
			    $('[name="id_provinsi"]').val(data.id_provinsi);

			    $('#modal_form').modal('show');

			    $('.modal-title').text('Update Data');

			},
			error: function (jqXHR, textStatus, errorThrown)
			{
			    alert('Error get data from ajax');
			}
			});
		}

	    function save()
	    {
	      	var url;
	      	if(save_method == 'add')
      		{
      			url = "<?php echo site_url('master/add_rsd') ?>";
	      	} else {
	        	url = "<?php echo site_url('master/update_rsd') ?>";
	    	}
	          $.ajax({
	            url : url,
	            type: "POST",
	            data: $('#form').serialize(),
	            dataType: "JSON",
	            success: function(data)
	            {
	               if(data.status)//if success close modal and reload ajax table
	               	{
	               		$('#modal_form').modal('hide');
	               		$('#messageSpan').append(data.message);

							  setTimeout(function () { // wait 3 seconds and reload
							  	window.location.reload(true); // for reload a page
							  }, 1000);
	          		}
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Error adding / update data');
	            }
	        });
	    }

        function del(id)
	    {
			if(confirm('Apakah Anda yakin data akan dihapus?'))
			{
			// ajax delete data from database
			  $.ajax({
			    url : "<?php echo site_url('master/del_rsd') ?>/"+id,
			    type: "POST",
			    dataType: "JSON",
			    success: function(data)
			    {
					$('#modal_form').modal('hide');
					$('#messageSpan').append(data.message);

							  setTimeout(function () { // wait 3 seconds and reload
							  	window.location.reload(true); // for reload a page
							  }, 1000);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			        alert('Error deleting data');
			    }
			  });
			}
	  	}
	</script>

    <!-- ============ MODAL UPDATE =============== -->

    <div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel"></h3>
        </div>
        <form class="form-horizontal" method="post" action="#" id="form">
            <div class="modal-body">
                <input type="hidden" name="id_rsd" value="" class="form-control" type="text" placeholder="ID RSD..." readonly>
                <div class="row form-group">
                    <label class="control-label col-xs-3" >Nama RSD</label>
                    <div class="col-xs-8">
                        <input name="nama_rsd" class="form-control" type="text" placeholder="Nama RSD..." required>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-xs-3" >Pemilik RSD</label>
                    <div class="col-xs-8">
                        <input name="pemilik" class="form-control" type="text" placeholder="Pemilik RSD..." required>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-xs-3" >Tipe RSD</label>
                    <div class="col-xs-8">
                        <select name="tipe" class="form-control" required>
                        	<option value="">--Pilih--</option>
                        	<option value="a">A</option>
                        	<option value="b">B</option>
                        	<option value="c">C</option>
                        	<option value="d">D</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="control-label col-xs-3" >Status BLUD RSD</label>
                    <div class="col-xs-8">
                        <select name="status" class="form-control" required>
                        	<option value="">--Pilih--</option>
                        	<option value="belum">Belum</option>
                        	<option value="bertahap">Bertahap</option>
                        	<option value="penuh">Penuh</option>

                        </select>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-xs-3" >Perwakilan</label>
                    <div class="col-xs-8">
                        <select name="id_perwakilan" class="form-control" required>
                        	<option value="">--Pilih Perwakilan--</option>
                        	<?php foreach ($perwakilan as $k) {
    echo '<option value="' . $k->id_perwakilan . '">' . $k->nama_perwakilan . '</option>';
}?>
                        </select>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="control-label col-xs-3" >Provinsi</label>
                    <div class="col-xs-8">
                        <select name="id_provinsi" class="form-control" required>
                        	<option value="">--Pilih Provinsi--</option>
                        	<?php foreach ($provinsi as $k) {
    echo '<option value="' . $k->id_provinsi . '">' . $k->nama_provinsi . '</option>';
}?>
                        </select>
                    </div>
                </div>

             </div>
        </form>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Tutup</button>
                <button class="btn" onclick="save()">Simpan</button>
            </div>
        </div>
        </div>
    </div>

    <?php //endforeach;?>
    <!--END MODAL-->