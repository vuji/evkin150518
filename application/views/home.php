<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Home</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="<?php echo site_url('home'); ?>">Home</a></li>
							</ol>
						</div>
					</div>
				</div>
			</header>


			<div class="row">
				<div class="col-xl-12">
	                <section class="card" style="padding: 30px;">

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-heading">
                    <h3 class="card-title">
                        <span class="glyphicon glyphicon-bookmark" style="color: pink;"></span> Tipe RSD</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <a href="javascript:void(0);" class="btn btn-success btn-xlarge" role="button"><span class="fa archive"> TIPE A</span> <br/>Jumlah : <?php if ($tipe_a == null) {echo "0";} else {foreach ($tipe_a as $res) {echo $res->jumlah;}}?></a>
                          <a href="javascript:void(0);" class="btn btn-primary btn-xlarge" role="button"><span class="fa archive"> TIPE B</span> <br/>Jumlah : <?php if ($tipe_b == null) {echo "0";} else {foreach ($tipe_b as $res) {echo $res->jumlah;}}?></a>
                          <a href="javascript:void(0);" class="btn btn-info btn-xlarge" role="button"><span class="fa archive"> TIPE C</span> <br/>Jumlah : <?php if ($tipe_c == null) {echo "0";} else {foreach ($tipe_c as $res) {echo $res->jumlah;}}?></a>
                          <a href="javascript:void(0);" class="btn btn-warning btn-xlarge" role="button"><span class="fa archive"> TIPE D</span> <br/>Jumlah : <?php if ($tipe_d == null) {echo "0";} else {foreach ($tipe_d as $res) {echo $res->jumlah;}}?></a>
                        </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div style="height: 30px;"></div>



 <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-heading">
                    <h3 class="card-title">
                        <span class="glyphicon glyphicon-bookmark" style="color: gray;"></span>                        Status RSD
</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                          <a href="javascript:void(0);" class="btn btn-success btn-xlarge" role="button"><span class="fa archive"> Belum</span> <br/><?php if ($status_belum == null) {echo "0";} else {foreach ($status_belum as $res) {echo $res->jumlah;}}?></a>
                          <a href="javascript:void(0);" class="btn btn-primary btn-xlarge" role="button"><span class="fa archive"> Bertahap</span> <br/>Jumlah : <?php if ($status_bertahap == null) {echo "0";} else {foreach ($status_bertahap as $ress) {echo $ress->jumlah;}}?></a>
                          <a href="javascript:void(0);" class="btn btn-info btn-xlarge" role="button"><span class="fa archive"> Penuh</span> <br/>Jumlah : <?php if ($status_penuh == null) {echo "0";} else {foreach ($status_penuh as $resss) {echo $resss->jumlah;}}?></a>

                        </div>
                </div>
            </div>
        </div>
    </div>

</div>

	                </section>
	            </div>
	        </div>
	    </div>
	</div>
