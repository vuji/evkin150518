	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Penilaian Aspek Pelayanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Hasil Evaluasi</a></li>
								<li class="active">Aspek Pelayanan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<form class="form form-horizontal" method="post" action="<?= site_url('hasilevaluasi/update_evaluasi_pelayanan')?>">
						
						<div class="row form-group">
								<label class="control-label col-md-4" >Tahun Buku</label>
								<div class="col-md-8">
									<span><?= $resultpelayanan[0]->tahun ?></span>
									<input type="hidden" name="tahun" value="<?= $resultpelayanan[0]->tahun ?>">
								</div>
							</div>
							<div class="row form-group">
								<label class="control-label col-md-4" >Nama RSD</label>
								<div class="col-md-8">
									<span><?= $resultpelayanan[0]->nama_rsd ?></span>
									<input type="hidden" name="id_rsd" value="<?= $resultpelayanan[0]->id_rsd ?>">
								</div>
							</div>

						
						<div class="table-responsive">
							<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
								<thead>
									<tr>
										<th>Subaspek</th> 	
										<th>Kelompok Indikator</th> 	
										<th>Indikator</th> 	
										<th>Bobot</th>
										<th>Skor</th>
										<th>% Capaian</th>
										<th>Penyebab</th>
									</tr>
								</thead>
								<tbody>
									<?php $namasubpek = $namaind = ''; ?>
									<?php if($resultpelayanan) { ?>
										<?php foreach ($resultpelayanan as $data) { ?>
											<tr>
												<td><?= $data->nama_subaspek!=$namasubpek ? $data->nama_subaspek : '';?></td>
												<td><?= $data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
												<td><?= $data->indikator_child_name;?></td>
												<td>
													<?= $data->bobot;?>
													<input type="hidden"  name="nilai[]" value="<?= $data->bobot ?>">
													<input type="hidden"  name="id_indikator[]" value="<?= $data->id_child_indikator ?>">
												</td>
											<td>
												<input type="text" size="5" name="capaian[]" value="<?= $data->capaian ?>">
											</td>
											<td>
												<span><?= round($data->persentase,2)*100 ?></span>

											</td>
											<td>
												<textarea name="penyebab[]"><?=  $data->penyebab ?></textarea>
											</td>
											</tr>
											<?php

											$namasubpek = $data->nama_subaspek;
											$namaind = $data->indikator_parent_name;

										} ?> 
									<?php } else { ?>

										<tr>
											<td  colspan="3" class="text-center">data tidak tersedia</td>
										</tr>

									<?php } ?> 
								</tbody>
							</table>
						</div>
						

						<button type="submit" class="btn btn-primary btn-xs pull-right">Evaluasi</button>


					</form>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->