	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Penilaian Aspek Keuangan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Hasil Evaluasi</a></li>
								<li class="active">Aspek Keuangan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="box-typical steps-numeric-block">

				<div class="card-block">
					<form id="penilaian" class="form form-horizontal" method="post" action="<?=site_url('hasilevaluasi/evaluasi_keuangan')?>">

							<div class="row form-group">
								<label class="control-label col-md-4" >Tahun Buku</label>
								<div class="col-md-8">
									<select class="form-control" name="tahun">
										<option value="" selected >--Tahun Buku--</option>
										<?php
$year = date("Y");
for ($x = 2016; $x <= $year; $x++) {
    echo "<option value=$x>$x</option>";
}
?>
									</select>
								</div>
							</div>
							<!-- <div class="row form-group">
								<label class="control-label col-md-4" >Perwakilan</label>
								<div class="col-md-8">
									<select class="form-control" name="id_perwakilan">
										<option value="" selected >--Pilih Perwakilan--</option>
										<?php foreach ($perwakilan as $k) {?>
											<option value="<?php echo $k->id_perwakilan; ?>" <?php echo ($perwakilann == $k->id_perwakilan) ? 'selected' : '' ?> ><?php echo $k->nama_perwakilan; ?></option>
											<?php }?>
									</select>
								</div>
							</div> -->
							<div class="row form-group">
								<label class="control-label col-md-4" >Nama RSD</label>
								<div class="col-md-8">
									<select name="id_rsd" class="form-control" required>
										<!-- <option value="">--Pilih RSD--</option>
										<?php if (count($dataRSD) != 0) {
    foreach ($dataRSD as $v) {?>
												<option value="<?php echo $v->id_rsd ?>" <?php echo ($rsd == $v->id_rsd) ? 'selected' : '' ?>><?php echo $v->nama_rsd ?>
												</option>
											<?php }}?> -->
											<option value="">--Pilih RSD--</option>
											<?php foreach ($rsd as $k) {
    echo '<option value="' . $k->id_rsd . '">' . $k->nama_rsd . '</option>';
}?>
									</select>
								</div>
							</div>


						<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nama Subaspek</th>
									<th>Nama Indikator</th>
									<th>Bobot</th>
									<th>Skor</th>
									<th>Penyebab</th>
								</tr>
							</thead>
							<tbody>
								<?php $nasubpek = '';?>
								<?php if ($result) {
    ?>
									<?php foreach ($result as $data) {?>
										<tr>
											<td><?=$data->nama_subaspek != $nasubpek ? $data->nama_subaspek : '';?></td>
											<td><?=$data->nama_indikator;?></td>
											<td>
												<?=$data->bobot;?>
													<input type="hidden" name="skor[]" value="<?=$data->bobot;?>">
													<input id = "indikator" type="hidden" name="id_indikator[]" value="<?=$data->id_indikator;?>">
											</td>
											<td>
													<input id="capaian" type="text" name="capaian[]" required="required" onkeyup="this.value = minmax(this.value, 0, <?=$data->bobot;?>)">
											</td>

											<td>
												<textarea name="penyebab[]" ></textarea>
											</td>
										</tr>
										<?php

        $nasubpek = $data->nama_subaspek;

    }?>
								<?php } else {?>

									<tr>
										<td  colspan="7" class="text-center">data tidak tersedia</td>
									</tr>

								<?php }?>
							</tbody>
						</table>


								<button type="submit" class="btn btn-primary btn-xs pull-right">Next</button>

								</br>
								</br>

								<button type="submit" class="btn btn-primary btn-xs pull-right">Evaluasi</button>


					</form>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script type="text/javascript">
		function minmax(value, min, max)
		{
		    if(parseFloat(value) < min || isNaN(parseFloat(value))) {
		        return '';
		    } else if(parseFloat(value) > max) {
		    	alert("Skor maksimal adalah "+max);
		        return max;
		    } else {
		    	return value;
		    };
		}

	</script>