	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Edit Penilaian Aspek Keuangan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Hasil Evaluasi</a></li>
								<li class="active">Aspek Keuangan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<form class="form form-horizontal" method="post" action="<?= site_url('hasilevaluasi/update_evaluasi_keuangan')?>">
						
							<div class="row form-group">
								<label class="control-label col-md-4" >Tahun Buku</label>
								<div class="col-md-8">
									<span><?= $result[0]->tahun ?></span>
									<input type="hidden" name="tahun" value="<?= $result[0]->tahun ?>">
								</div>
							</div>
							<div class="row form-group">
								<label class="control-label col-md-4" >Nama RSD</label>
								<div class="col-md-8">
									<span><?= $result[0]->nama_rsd ?></span>
									<input type="hidden" name="id_rsd" value="<?= $result[0]->id_rsd ?>">
								</div>
							</div>
							
						<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nama Subaspek</th> 	
									<th>Nama Indikator</th> 	
									<th>Bobot</th>
									<th>Skor</th>
									<th>%Capaian</th>
									<th>Penyebab</th>
								</tr>
							</thead>
							<tbody>
								<?php $nasubpek = ''; ?>
								<?php if($result) { ?>
									<?php foreach ($result as $data) { ?>
										<tr>
											<td><?= $data->nama_subaspek!=$nasubpek ? $data->nama_subaspek : '';?></td>
											<td><?= $data->nama_indikator;?></td>
											<td>
												<?= $data->bobot;?>
													<input type="hidden" name="skor[]" value="<?= $data->bobot;?>">
													<input type="hidden" name="id_indikator[]" value="<?= $data->id_indikator;?>">
											</td>
											<td>
												<input type="text" size="5" name="capaian[]" value="<?= $data->capaian ?>">
											</td>
											<td>
												<span><?= round($data->persentase,2)*100 ?></span>

											</td>
											<td>
												<textarea name="penyebab[]"><?=  $data->penyebab ?></textarea>
											</td>
										</tr>
										<?php

										$nasubpek = $data->nama_subaspek;

									} ?> 
								<?php } else { ?>

									<tr>
										<td  colspan="7" class="text-center">data tidak tersedia</td>
									</tr>

								<?php } ?> 
							</tbody>
						</table>
						
							
								<button type="submit" class="btn btn-primary btn-xs pull-right">Evaluasi</button>
							
					
					</form>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->