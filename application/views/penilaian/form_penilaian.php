	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Penilaian Aspek Pelayanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Hasil Evaluasi</a></li>
								<li class="active">Form Penilaian</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">


<div class="container">
	<form role="form" id="form-penilian" method="post" action="<?=site_url('hasilevaluasi/proses_evaluasi')?>">

						<div class="row form-group">
							<label class="control-label col-md-4" >Tahun Buku</label>
							<div class="col-md-8">
								<select class="form-control" name="tahun" required="">
									<option value="" selected >--Tahun Buku--</option>
										<?php
$year = date("Y");
for ($x = 2016; $x <= $year; $x++) {
    echo "<option value=$x>$x</option>";
}
?>
								</select>
							</div>
						</div>

						<!-- <div class="row form-group">
							<label class="control-label col-md-4" >Perwakilan</label>
							<div class="col-md-8">
								<select name="id_rsd" class="form-control" required="">
									<option value="">--Pilih Perwakilan--</option>
									<?php foreach ($perwakilan as $k) {
    echo '<option value="' . $k->id_perwakilan . '">' . $k->nama_perewakilan . '</option>';
}?>
								</select>
							</div>
						</div> -->

						<div class="row form-group">
							<label class="control-label col-md-4" >Nama RSD</label>
							<div class="col-md-8">
								<select name="id_rsd" class="form-control" required="">
									<option value="">--Pilih RSD--</option>
									<?php foreach ($rsd as $k) {
    echo '<option value="' . $k->id_rsd . '">' . $k->nama_rsd . '</option>';
}?>
								</select>
							</div>
						</div>

<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" class="btn btn-primary btn-circle">1</a>
            <p>Data Keuangan</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Data Layanan</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Selesai</p>
        </div>
    </div>
</div>

	    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Data Keuangan</h3>
                <table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nama Subaspek</th>
									<th>Nama Indikator</th>
									<th>Bobot</th>
									<th>Skor</th>
									<th>Penyebab</th>
								</tr>
							</thead>
							<tbody>
								<?php $nasubpek = '';?>
								<?php if ($result_keuangan) {
    ?>
									<?php foreach ($result_keuangan as $data) {
        ?>
										<tr>
											<td><?=$data->nama_subaspek != $nasubpek ? $data->nama_subaspek : '';?></td>
											<td><?=$data->nama_indikator;?></td>
											<td>
												<?=$data->bobot;?>
													<input type="hidden" name="skor_keuangan[]" value="<?=$data->bobot;?>">
													<input id = "indikator" type="hidden" name="id_indikator_keuangan[]" value="<?=$data->id_indikator;?>">
											</td>
											<td>
													<input id="capaian" type="number" name="capaian_keuangan[]" required="required" onkeyup="this.value = minmax(this.value, 0, <?=$data->bobot;?>)" step="any">
											</td>

											<td>
												<textarea name="penyebab_keuangan[]" ></textarea>
											</td>
										</tr>
										<?php

        $nasubpek = $data->nama_subaspek;

    }?>
								<?php } else {?>

									<tr>
										<td  colspan="7" class="text-center">data tidak tersedia</td>
									</tr>

								<?php }?>
							</tbody>
						</table>
						<div style="margin-bottom: 30px;"></div>
                <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Data Layanan</h3>
                <div class="table-responsive">
							<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
								<thead>
									<tr>
										<th>Subaspek</th>
										<th>Kelompok Indikator</th>
										<th>Indikator</th>
										<th>Bobot</th>
										<th>Skor</th>
										<th>Penyebab</th>
									</tr>
								</thead>
								<tbody>
									<?php $namasubpek = $namaind = '';?>
									<?php if ($result_pelayanan) {
    ?>
										<?php foreach ($result_pelayanan as $data) {
        ?>
											<tr>
												<td><?=$data->nama_subaspek != $namasubpek ? $data->nama_subaspek : '';?></td>
												<td><?=$data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
												<td><?=$data->indikator_child_name;?></td>
												<td>
													<?=$data->bobot;?>
													<input type="hidden"  name="nilai_pelayanan[]" value="<?=$data->bobot?>">
													<input type="hidden"  name="id_indikator_pelayanan[]" value="<?=$data->id_child_indikator?>">
												</td>
												<td><input id= capaiann type="number" name="capaian_pelayanan[]" size="6" onkeyup="this.value = minmax(this.value, 0, <?=$data->bobot;?>)"  step="any"  required=""></td>
												<td><textarea name="penyebab_pelayanan[]"></textarea></td>
											</tr>
											<?php

        $namasubpek = $data->nama_subaspek;
        $namaind = $data->indikator_parent_name;

    }?>
									<?php } else {?>

										<tr>
											<td  colspan="3" class="text-center">data tidak tersedia</td>
										</tr>

									<?php }?>
								</tbody>
							</table>
						</div>
						<div style="margin-bottom: 30px;">

						</div>
                <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
            </div>
        </div>
    </div>

    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3 class="text-center"> Apakah anda yakin dengan data ini? klik selsai jika iya</h3>
                <button class="btn btn-success btn-sm pull-right" type="submit">Selesa!</button>
            </div>
        </div>
    </div>

    </form>

</div>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<script src="<?=base_url('assets/js/lib/parsley.min.js')?>"></script>
<script src="<?=base_url('assets/js/lib/parsley-i18n/id.js')?>"></script>

<script type="text/javascript">
	var $form = $('#form-penilian');
	  $form.parsley().validate();
</script>
	<script type="text/javascript">
		function minmax(value,min,max)
		{
			if(parseFloat(value) < min || isNaN(parseFloat > (value ))){
				return '';
			} else if (parseFloat(value)> max){
				alert ("Skor maksimal adalah "+max);
				return max;
			} else{
				return value;
			};
		}



		$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-danger");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-danger");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
	</script>
