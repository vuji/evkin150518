	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Penilaian Aspek Pelayanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Hasil Evaluasi</a></li>
								<li class="active">Aspek Pelayanan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<form class="form form-horizontal" method="post" action="<?= site_url('hasilevaluasi/evaluasi_pelayanan')?>">
						
						<div class="row form-group">
							<label class="control-label col-md-4" >Tahun Buku</label>
							<div class="col-md-8">
								<select class="form-control" name="tahun">
									<option value="" selected >--Tahun Buku--</option>
									<?php 
									$year=date("Y");
									for($x=2016; $x <= $year; $x++)
									{
										echo "<option value=$x>$x</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="row form-group">
							<label class="control-label col-md-4" >Nama RSD</label>
							<div class="col-md-8">
								<select name="id_rsd" class="form-control" required>
									<option value="">--Pilih RSD--</option>
									<?php foreach($rsd as $k){
										echo '<option value="'.$k->id_rsd.'">'.$k->nama_rsd.'</option>';
									} ?>
								</select>
							</div>
						</div>

						
						<div class="table-responsive">
							<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
								<thead>
									<tr>
										<th>Subaspek</th> 	
										<th>Kelompok Indikator</th> 	
										<th>Indikator</th> 	
										<th>Bobot</th>
										<th>Skor</th>
										<th>Penyebab</th>
									</tr>
								</thead>
								<tbody>
									<?php $namasubpek = $namaind = ''; ?>
									<?php if($resultpelayanan) { ?>
										<?php foreach ($resultpelayanan as $data) { ?>
											<tr>
												<td><?= $data->nama_subaspek!=$namasubpek ? $data->nama_subaspek : '';?></td>
												<td><?= $data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
												<td><?= $data->indikator_child_name;?></td>
												<td>
													<?= $data->bobot;?>
													<input type="hidden"  name="nilai[]" value="<?= $data->bobot ?>">
													<input type="hidden"  name="id_indikator[]" value="<?= $data->id_child_indikator ?>">
												</td>
												<td><input id= capaiann type="text" name="capaian[]" size="6" required= "required" onkeyup="this.value = minmax(this.value, 0, <?= $data->bobot; ?>)"></td>
												<td><textarea name="penyebab[]"></textarea></td>
											</tr>
											<?php

											$namasubpek = $data->nama_subaspek;
											$namaind = $data->indikator_parent_name;

										} ?> 
									<?php } else { ?>

										<tr>
											<td  colspan="3" class="text-center">data tidak tersedia</td>
										</tr>

									<?php } ?> 
								</tbody>
							</table>
						</div>
						

						<button type="submit" class="btn btn-primary btn-xs pull-right">Evaluasi</button>


					</form>
				</div>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->
	<script type="text/javascript">
		function minmax(value,min,max)
		{
			if(parseFloat(value) < min || isNaN(parseFloat > (value ))){
				return '';
			} else if (parseFloat(value)> max){
				alert ("Skor maksimal adalah "+max);
				return max;
			} else{
				return value;	
			};
		}
	</script>