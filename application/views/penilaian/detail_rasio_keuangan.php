	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Detail Penilaian </h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="hasilevaluasi/rasiokeuangan">Hasil Evaluasi</a></li>
								<li class="active">Aspek Keuangan & Pelayanan</li>
							</ol>
						</div>
					</div>
				</div>
			</header>

			<section class="card">


				<div class="card">
  <div class="card-header">
    <div class="row form-group">
								<label class="control-label col-md-4" >Tahun Buku</label>
								<div class="col-md-8">
									<span><?=$result[0]->tahun?></span>
									<input type="hidden" name="tahun" value="<?=$result[0]->tahun?>">
								</div>
							</div>
							<div class="row form-group">
								<label class="control-label col-md-4" >Nama RSD</label>
								<div class="col-md-8">
									<span><?=$result[0]->nama_rsd?></span>
									<input type="hidden" name="id_rsd" value="<?=$result[0]->id_rsd?>">
								</div>
							</div>
  </div>
  <div class="card-header">
<div>
 <!--  <ul class="nav nav-tabs">
    <li class="active nav-item""><a href="#first" data-toggle="tab">1</a></li>
    <li class="nav-item""><a href="#second" data-toggle="tab">2</a></li>
    <li class="nav-item""><a href="#third" data-toggle="tab">3</a></li>
    <li class="nav-item""><a href="#fourth" data-toggle="tab">4</a></li>

  </ul>
 -->
  <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Data Keuangan</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Data Layanan</a>
  </li>

</ul>
</div>
<div class="tab-content">
  <div class="tab-pane active" id="first">
      <table id="tabel" class="display table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nama Subaspek</th>
									<th>Nama Indikator</th>
									<th>Bobot</th>
									<th>Skor</th>
									<th>%Capaian</th>
									<th>Penyebab</th>
								</tr>
							</thead>
							<tbody>
								<?php $nasubpek = '';?>
								<?php if ($result) {
    ?>
									<?php foreach ($result as $data) {
        ?>
										<tr>
											<td><?=$data->nama_subaspek != $nasubpek ? $data->nama_subaspek : '';?></td>
											<td><?=$data->nama_indikator;?></td>
											<td>
												<?=$data->bobot;?>
													<input type="hidden" name="skor[]" value="<?=$data->bobot;?>">
													<input type="hidden" name="id_indikator[]" value="<?=$data->id_indikator;?>">
											</td>
											<td>
												<?=$data->capaian;?>
											</td>
											<td>
												<span><?=round($data->persentase, 2) * 100?></span>

											</td>
											<td>
												<?=$data->penyebab;?>
											</td>
										</tr>
										<?php

        $nasubpek = $data->nama_subaspek;

        $total_bobot += $data->bobot;
        $total_skor += $data->capaian;

    }?>
								<?php } else {
    ?>

									<tr>
										<td  colspan="7" class="text-center">data tidak tersedia</td>
									</tr>

								<?php }?>


								<tr style="background: #e4ecf1;">
									<td colspan="2">Total</td>
									<td>
										<?php if ($total_bobot == 0) {
    echo "0";
} else {
    echo ($total_bobot);
}?>
									</td>

									<td colspan="3">
										<?php if ($total_skor == 0) {
    echo "0";
} else {
    echo ($total_skor);
}?>


									</td>
								</tr>

							</tbody>
						</table>
  </div>
  <div class="tab-pane" id="second">
    <div class="table-responsive">
							<table id="tabel" class="display table table-bordered" cellspacing="0" width="100%" style="font-size: 11px;">
								<thead>
									<tr>
										<th>Subaspek</th>
										<th>Kelompok Indikator</th>
										<th>Indikator</th>
										<th>Bobot</th>
										<th>Skor</th>
										<th>% Capaian</th>
										<th>Penyebab</th>
									</tr>
								</thead>
								<tbody>
									<?php $namasubpek = $namaind = '';?>
									<?php if ($resultpelayanan) {
    ?>
										<?php foreach ($resultpelayanan as $data) {
        ?>
											<tr>
												<td><?=$data->nama_subaspek != $namasubpek ? $data->nama_subaspek : '';?></td>
												<td><?=$data->indikator_parent_name != $namaind ? $data->indikator_parent_name : '';?></td>
												<td><?=$data->indikator_child_name;?></td>
												<td>
													<?=$data->bobot;?>
													<input type="hidden"  name="nilai[]" value="<?=$data->bobot?>">
													<input type="hidden"  name="id_indikator[]" value="<?=$data->id_child_indikator?>">
												</td>
											<td>

												<span><?=$data->capaian?></span>
											</td>
											<td>
												<span><?=round($data->persentase, 2) * 100?></span>

											</td>
											<td>
												<span><?=$data->penyebab?></span>
											</td>
											</tr>
											<?php

        $namasubpek = $data->nama_subaspek;
        $namaind = $data->indikator_parent_name;

    }?>
									<?php } else {?>

										<tr>
											<td  colspan="3" class="text-center">data tidak tersedia</td>
										</tr>

									<?php }?>
								</tbody>
							</table>
						</div>

  </div>

</div>
  </div>
</div>

<a href="<?php echo base_url() ?>export/exl_rsd_item/<?=$result[0]->id_rsd?>/<?=$result[0]->tahun?>" class="btn btn-primary float-right"><i class="fa fa-file-excel-o"></i> Export</a>
			</section><!--.box-typical-->
		</div><!--.container-fluid-->
	</div><!--.page-content-->